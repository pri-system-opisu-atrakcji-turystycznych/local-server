package uam.server.local.spring.configuration.development;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Profile("development")
@Configuration
@EnableWebSecurity(debug = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private DataSource dataSource;

    @Autowired
    public SecurityConfiguration(BCryptPasswordEncoder bCryptPasswordEncoder, DataSource dataSource) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.dataSource = dataSource;

    }

    @Value("select email, password, active from user where email=?")
    private String usersQuery;

    @Value("select u.email, r.role from user u inner join user_role ur on(u.id=ur.id) inner join role r on(ur.role_id=r.role_id) where u.email=?")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests()

                .antMatchers("/server/**").permitAll()
                .antMatchers("/descriptions/element/addDescription").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/descriptions/**").permitAll()
                .antMatchers("/institutions/addInstitution").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/institutions/**").permitAll()
                .antMatchers("/messages/addMessage").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/messages/**").permitAll()
                .antMatchers("/elements/addElement").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/elements/**").permitAll()
                .antMatchers("/routes/addRoute").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/routes/**").permitAll()
                .antMatchers("/routes-elements/route/addRouteElement").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/routes-elements/**").permitAll()
                .antMatchers("/questions/addQuestion").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/questions/**").permitAll()
                .antMatchers("/tips/addTip").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/tips/**").permitAll()
                .antMatchers("/image/**").permitAll()
                .antMatchers("/sound/**").permitAll()
                .antMatchers("/prize/addPrizes").hasAnyAuthority("ADMIN", "REDAKTOR")
                .antMatchers("/prize/**").permitAll()

                .antMatchers("/login").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/registration").permitAll()

                .antMatchers("/user/**").hasAnyAuthority("ADMIN", "REDAKTOR").anyRequest()

                .authenticated().and().formLogin()
                .loginPage("/login")
                .failureUrl("/login?error=true")
                .defaultSuccessUrl("/")
                .usernameParameter("email")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/").and().exceptionHandling()
                .accessDeniedPage("/access-denied")
                .and().formLogin()

                .and().csrf().ignoringAntMatchers("/h2-console/**")
                .and()
                .rememberMe()
                .key("myUniqueKey")
                .rememberMeCookieName("websparrow-login-remember-me")
                .tokenValiditySeconds(36000)
                .and().headers().frameOptions().sameOrigin();

    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/resources/**", "/assets/**", "/css/**", "/js/**", "/images/**", "/fonts/**");
    }

}
