package uam.server.local.spring.service;

import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;
import uam.server.local.model.TipModel;

public interface ElementService extends Service<ElementModel> {

    Iterable<ElementModel> getAll();

    ElementModel getElementById(final Long id);

    ElementModel getElementFromDescription(DescriptionModel model);

    void delete(ElementModel model);

    void clearTip(TipModel tip);

    Iterable<ElementModel> getAllNotRemoved();

    void deleteById(Long id);
}
