package uam.server.local.spring.service.impl;

import uam.server.local.exception.PrizeNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import uam.server.local.model.PrizeModel;
import uam.server.local.spring.repository.PrizeRepository;
import uam.server.local.spring.service.PrizeService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class PrizeServiceImpl implements PrizeService {

    @Resource
    private PrizeRepository prizeRepository;

    @Override
    public synchronized PrizeModel getPrize() {
        PrizeModel prize = prizeRepository.findTopByActiveIsTrue().orElse(new PrizeModel(null, null, null));
        prize.setActive(false);
        prizeRepository.save(prize);
        return prize;
    }

    @Override
    public void addPrizes(final int quantity) {
        for (int i = 0; i < quantity; i++) {
            prizeRepository.save(new PrizeModel(null, generateNewPrizeCode(), true));
        }
    }

    @Override
    public Iterable<PrizeModel> getPrizes() {
        return prizeRepository.findAllByActiveIsTrueOrderByIdDesc();
    }

    @Override
    public void deleteById(Long id) {
        Optional<PrizeModel> prize = Optional.ofNullable(getPrizeById(id));
        prize.get().setActive(false);
        prizeRepository.save(prize.get());
    }

    private PrizeModel getPrizeById(Long id) {
        return prizeRepository.findById(id)
                .orElseThrow(() -> new PrizeNotFoundException(id));
    }


    private String generateNewPrizeCode() {
        return RandomStringUtils.random(6, true, true);
    }

}
