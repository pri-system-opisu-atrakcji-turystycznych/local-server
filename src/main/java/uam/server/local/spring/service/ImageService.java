package uam.server.local.spring.service;

import uam.server.local.model.ImageModel;

public interface ImageService extends Service<ImageModel> {

    Iterable<ImageModel> getAll();

    ImageModel getImageById(final Long id);

}
