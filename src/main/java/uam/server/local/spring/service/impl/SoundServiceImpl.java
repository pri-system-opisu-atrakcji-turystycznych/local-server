package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.SoundNotFoundException;
import uam.server.local.model.SoundModel;
import uam.server.local.spring.repository.SoundRepository;
import uam.server.local.spring.service.SoundService;

import javax.annotation.Resource;

@Service
public class SoundServiceImpl implements SoundService {

    @Resource
    private SoundRepository soundRepository;

    @Override
    public SoundModel getSoundById(final Long id) {
        return soundRepository.findTopById(id)
                .orElseThrow(() -> new SoundNotFoundException(id));
    }

    @Override
    public Iterable<SoundModel> getAll() {
        return soundRepository.findAll();
    }

    @Override
    public SoundModel save(final SoundModel routeElement) throws Exception {
        return null;
    }
}
