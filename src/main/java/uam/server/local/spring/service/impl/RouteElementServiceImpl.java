package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.RouteElementNotFoundException;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.repository.RouteElementRepository;
import uam.server.local.spring.service.RouteElementService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class RouteElementServiceImpl implements RouteElementService {

    @Resource
    private RouteElementRepository routeElementRepository;

    @Override
    public Iterable<RouteElementModel> getAll() {
        return routeElementRepository.findAll();
    }

    @Override
    public Iterable<RouteElementModel> getRouteElementByRouteId(final Long id) {
        return routeElementRepository.findByRoute_Id(id);
    }

    @Override
    public RouteElementModel save(final RouteElementModel routeElementModel) {
        return routeElementRepository.save(routeElementModel);
    }

    @Override
    public void delete(RouteElementModel model) {
        routeElementRepository.delete(model);
    }

    @Override
    public void deleteById(long id) {
        Optional<RouteElementModel> routeElement = Optional.ofNullable(getRouteElementById(id));
        routeElement.get().setRemoved(true);
        routeElementRepository.save(routeElement.get());
    }

    @Override
    public RouteElementModel getRouteElementById(Long id) {
        return routeElementRepository.findById(id).orElseThrow(() -> new RouteElementNotFoundException(id));
    }

    @Override
    public Iterable<RouteElementModel> getAllNotRemoved() {
        return routeElementRepository.getAllNotRemoved();
    }


}
