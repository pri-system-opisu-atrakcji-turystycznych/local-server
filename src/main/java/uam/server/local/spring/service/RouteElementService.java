package uam.server.local.spring.service;

import uam.server.local.model.RouteElementModel;

public interface RouteElementService extends Service<RouteElementModel> {

    Iterable<RouteElementModel> getAll();

    Iterable<RouteElementModel> getRouteElementByRouteId(final Long id);

    RouteElementModel save(final RouteElementModel routeElementModel);

    void delete(RouteElementModel model);

    void deleteById(long id);

    RouteElementModel getRouteElementById(Long id);

    Iterable<RouteElementModel> getAllNotRemoved();
}
