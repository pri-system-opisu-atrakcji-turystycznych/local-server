package uam.server.local.spring.service;

import uam.server.local.model.SoundModel;

public interface SoundService extends Service<SoundModel> {

    Iterable<SoundModel> getAll();

    SoundModel getSoundById(final Long id);

}