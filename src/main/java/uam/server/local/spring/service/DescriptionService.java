package uam.server.local.spring.service;

import uam.server.local.model.DescriptionModel;

public interface DescriptionService extends Service<DescriptionModel> {

    Iterable<DescriptionModel> getAll();

    DescriptionModel getDescriptionByElementIdAndMobileIsTrue(final Long id);

    DescriptionModel getDescriptionByElementIdAndWebIsTrue(final Long id);

    DescriptionModel getDescriptionByElementIdAndWebIsTrueAndMobileIsTrue(final Long id);

    DescriptionModel getDescriptionByElementIdAndWebIsFalseAndMobileIsFalse(final Long id);

    void delete(Long id);

    DescriptionModel getDescriptionById(Long id);
}
