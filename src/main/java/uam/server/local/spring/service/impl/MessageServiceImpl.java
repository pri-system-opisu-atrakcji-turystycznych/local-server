package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.MessageNotFoundException;
import uam.server.local.model.MessageModel;
import uam.server.local.spring.repository.MessageRepository;
import uam.server.local.spring.service.MessageService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageRepository messageRepository;

    @Override
    public Iterable<MessageModel> getAll() {
        return messageRepository.findAll();
    }

    @Override
    public MessageModel save(final MessageModel messageModel) {
        messageModel.setRemoved(false);
        return messageRepository.save(messageModel);
    }

    @Override
    public void delete(MessageModel model) {
        messageRepository.delete(model);
    }

    @Override
    public void deleteById(Long id) {
        Optional<MessageModel> message = Optional.ofNullable(getMessageById(id));
        message.get().setRemoved(true);
        messageRepository.save(message.get());

    }

    public MessageModel getMessageById(Long id) {
        return messageRepository.findById(id)
                .orElseThrow(() -> new MessageNotFoundException(id));
    }
}
