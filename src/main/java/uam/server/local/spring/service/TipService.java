package uam.server.local.spring.service;

import uam.server.local.model.TipModel;

public interface TipService extends Service<TipModel> {

    Iterable<TipModel> getAll();

    TipModel getTipById(final Long id);

    TipModel save(final TipModel tipModel);

    void deleteById(Long id);

    Iterable<TipModel> getAllNotRemoved();
}
