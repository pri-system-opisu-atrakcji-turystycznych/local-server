package uam.server.local.spring.service;

import uam.server.local.model.InstitutionModel;

public interface InstitutionService extends Service<InstitutionModel> {

    Iterable<InstitutionModel> getAll();

    InstitutionModel getInstitutionByNameIsNotNull();

    InstitutionModel save(final InstitutionModel institutionModel);

    void deleteById(Long id);

    InstitutionModel getInstitutionById(Long id);
}
