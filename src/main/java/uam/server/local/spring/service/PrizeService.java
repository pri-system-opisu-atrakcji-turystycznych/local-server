package uam.server.local.spring.service;

import uam.server.local.model.PrizeModel;

public interface PrizeService {

    PrizeModel getPrize();

    void addPrizes(final int quantity);

    Iterable<PrizeModel> getPrizes();

    void deleteById(Long id);
}
