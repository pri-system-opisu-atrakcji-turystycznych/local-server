package uam.server.local.spring.service;

import uam.server.local.model.RouteModel;

public interface RouteService extends Service<RouteModel> {

    Iterable<RouteModel> getAll();

    RouteModel getRouteById(final Long id);
    
    RouteModel save(final RouteModel model) throws Exception;

    void delete(RouteModel model);

    void deleteById(Long id);

    Iterable<RouteModel> getAllNotRemoved();
}
