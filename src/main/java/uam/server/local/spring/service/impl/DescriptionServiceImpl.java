package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.DescriptionNotFoundException;
import uam.server.local.exception.DescriptionParameterNotFoundException;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;
import uam.server.local.spring.repository.DescriptionRepository;
import uam.server.local.spring.repository.ElementRepository;
import uam.server.local.spring.service.DescriptionService;

import javax.annotation.Resource;
import javax.naming.SizeLimitExceededException;
import java.util.Optional;

@Service
public class DescriptionServiceImpl implements DescriptionService {

    @Resource
    private DescriptionRepository descriptionRepository;
    @Resource
    private ElementRepository elementRepository;

    @Override
    public Iterable<DescriptionModel> getAll() {
        return descriptionRepository.findAll();
    }

    @Override
    public DescriptionModel getDescriptionByElementIdAndMobileIsTrue(final Long id) {
        return descriptionRepository.findFirstByElement_IdAndMobileIsTrue(id)
                .orElseThrow(() -> new DescriptionNotFoundException(id));
    }

    @Override
    public DescriptionModel getDescriptionByElementIdAndWebIsTrue(final Long id) {
        return descriptionRepository.findFirstByElement_IdAndWebIsTrue(id)
                .orElseThrow(() -> new DescriptionNotFoundException(id));
    }

    @Override
    public DescriptionModel getDescriptionByElementIdAndWebIsTrueAndMobileIsTrue(final Long id) {
        return descriptionRepository.findFirstByElement_IdAndWebIsTrueAndMobileIsTrue(id)
                .orElseThrow(() -> new DescriptionNotFoundException(id));
    }

    @Override
    public DescriptionModel getDescriptionByElementIdAndWebIsFalseAndMobileIsFalse(final Long id) {
        return descriptionRepository.findFirstByElement_IdAndWebIsFalseAndMobileIsFalse(id)
                .orElseThrow(() -> new DescriptionNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        Optional<DescriptionModel> description = Optional.ofNullable(getDescriptionById(id));
        description.get().setRemoved(true);
        descriptionRepository.save(description.get());
    }

    @Override
    public DescriptionModel getDescriptionById(Long id) {
        return descriptionRepository.findById(id).orElseThrow(() -> new DescriptionNotFoundException(id));
    }

    @Override
    public DescriptionModel save(final DescriptionModel descriptionModel) throws Exception {
        descriptionModel.setElement(getElementToJoin(descriptionModel.getElement()));
        descriptionModel.setRemoved(false);
        validateSavingDescription(descriptionModel);
        return descriptionRepository.save(descriptionModel);
    }

    private ElementModel getElementToJoin(final ElementModel elementModel) {
        try {
            return elementRepository.findById(elementModel.getId()).get();
        } catch (Exception e) {
            return null;
        }
    }

    private void validateSavingDescription(final DescriptionModel descriptionModel) throws Exception {
        if (descriptionModel == null) {
            throw new NullPointerException();
        }
        if (descriptionModel.getContent().length() > 1000) {
            throw new SizeLimitExceededException();
        }
        if (descriptionModel.getTitle() == null || descriptionModel.getElement() == null || descriptionModel.getWeb() == null || descriptionModel.getMobile() == null) {
            throw new DescriptionParameterNotFoundException(descriptionModel);
        }
    }
}
