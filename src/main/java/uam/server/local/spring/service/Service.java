package uam.server.local.spring.service;

public interface Service<ModelType> {

    ModelType save(ModelType routeElement) throws Exception;

}
