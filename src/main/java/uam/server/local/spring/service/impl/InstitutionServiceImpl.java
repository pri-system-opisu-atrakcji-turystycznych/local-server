package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.InstitutionNotFoundException;
import uam.server.local.model.InstitutionModel;
import uam.server.local.spring.repository.InstitutionRepository;
import uam.server.local.spring.service.InstitutionService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class InstitutionServiceImpl implements InstitutionService {

    @Resource
    private InstitutionRepository institutionRepository;

    @Override
    public Iterable<InstitutionModel> getAll() {
        return institutionRepository.findAll();
    }

    @Override
    public InstitutionModel getInstitutionByNameIsNotNull() {
        return institutionRepository.findFirstByNameIsNotNull()
                .orElseThrow(InstitutionNotFoundException::new);
    }

    @Override
    public InstitutionModel save(final InstitutionModel institutionModel) {
        institutionModel.setRemoved(false);
        return institutionRepository.save(institutionModel);

    }

    @Override
    public void deleteById(Long id) {
        Optional<InstitutionModel> institution = Optional.ofNullable(getInstitutionById(id));
        institution.get().setRemoved(true);
        institutionRepository.save(institution.get());

    }

    @Override
    public InstitutionModel getInstitutionById(Long id) {
        return institutionRepository.findById(id)
                .orElseThrow(() -> new InstitutionNotFoundException());
    }
}
