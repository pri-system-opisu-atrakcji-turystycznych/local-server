package uam.server.local.spring.service;

import uam.server.local.model.MessageModel;

public interface MessageService extends Service<MessageModel> {

    Iterable<MessageModel> getAll();

    MessageModel save(final MessageModel institutionModel);

    void delete(MessageModel model);

    void deleteById(Long id);

    MessageModel getMessageById(Long id);
}
