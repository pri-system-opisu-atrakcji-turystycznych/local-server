package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.RouteNotFoundException;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.repository.RouteRepository;
import uam.server.local.spring.service.RouteService;

import javax.annotation.Resource;
import javax.management.AttributeNotFoundException;
import java.util.Optional;

@Service
public class RouteServiceImpl implements RouteService {

    @Resource
    private RouteRepository routeRepository;

    @Override
    public Iterable<RouteModel> getAll() {
        return routeRepository.findAll();
    }

    @Override
    public RouteModel getRouteById(final Long id) {
        return routeRepository.findTopById(id)
                .orElseThrow(() -> new RouteNotFoundException(id));
    }

    public RouteModel save(final RouteModel routeModel) throws Exception {
        validateSavingRoute(routeModel);
        routeModel.setRemoved(false);
        return routeRepository.save(routeModel);
    }

    @Override
    public void delete(RouteModel model) {
        routeRepository.delete(model);
    }

    @Override
    public void deleteById(Long id) {
        Optional<RouteModel> route = Optional.ofNullable(getRouteById(id));
        route.get().setRemoved(true);
        routeRepository.save(route.get());
    }

    @Override
    public Iterable<RouteModel> getAllNotRemoved() {
        return routeRepository.getAllNotRemoved();
    }

    private void validateSavingRoute(final RouteModel routeModel) throws Exception {
        if (routeModel == null) {
            throw new NullPointerException();
        }
        if (routeModel.getDescription() == null || routeModel.getName() == null) {
            throw new AttributeNotFoundException();
        }
    }

}
