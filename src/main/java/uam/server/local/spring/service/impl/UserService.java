package uam.server.local.spring.service.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uam.server.local.exception.RoleNotFoundException;
import uam.server.local.exception.UserNotFoundException;
import uam.server.local.model.RoleModel;
import uam.server.local.model.UserModel;
import uam.server.local.spring.repository.RoleRepository;
import uam.server.local.spring.repository.UserRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class UserService {

    private static final String ROLE_OF_THE_SMALLEST_ACCESS = "REDAKTOR";

    @Resource
    private UserRepository userRepository;
    @Resource
    private RoleRepository roleRepository;
    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserModel findUserByEmail(final String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    public Boolean isExist(final String email) {
        return userRepository.existsByEmail(email);
    }

    public synchronized UserModel saveUser(final UserModel user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1L);
        user.setRoles(getBestRoles((HashSet<RoleModel>) user.getRoles()));
        return userRepository.save(user);
    }

    private HashSet<RoleModel> getBestRoles(final HashSet<RoleModel> roles) {
        if (roles == null) {
            RoleModel role = roleRepository.findByRole(ROLE_OF_THE_SMALLEST_ACCESS)
                    .orElseThrow(() -> new RoleNotFoundException(ROLE_OF_THE_SMALLEST_ACCESS));
            return new HashSet<>(Arrays.asList((role)));
        } else {
            List<RoleModel> rolesToAdd = new ArrayList<>();
            for (RoleModel r : roles) {
                String searchingRole = r.getRole();
                RoleModel role = roleRepository.findByRole(searchingRole)
                        .orElseThrow(() -> new RoleNotFoundException(searchingRole));
                if (role == null) {
                    rolesToAdd.add(roleRepository.findByRole(ROLE_OF_THE_SMALLEST_ACCESS)
                            .orElseThrow(() -> new RoleNotFoundException(ROLE_OF_THE_SMALLEST_ACCESS)));

                } else {
                    rolesToAdd.add(role);
                }
            }
            return new HashSet<>(rolesToAdd);
        }
    }

    public void updateUser(UserModel user) {
        userRepository.save(user);
    }

    public void updateUserByAdmin(UserModel user) {
        user.setRoles(getBestRoles((HashSet<RoleModel>) user.getRoles()));
        userRepository.save(user);
    }
}
