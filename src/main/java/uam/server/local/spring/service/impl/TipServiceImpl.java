package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.TipNotFoundException;
import uam.server.local.model.TipModel;
import uam.server.local.spring.repository.TipRepository;
import uam.server.local.spring.service.TipService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class TipServiceImpl implements TipService {

    @Resource
    private TipRepository tipRepository;

    @Override
    public Iterable<TipModel> getAll() {
        return tipRepository.findAll();
    }

    @Override
    public TipModel save(final TipModel tipModel) {
        tipModel.setRemoved(false);
        return tipRepository.save(tipModel);

    }


    @Override
    public void deleteById(Long id) {
        TipModel tip = getTipById(id);
        tip.setRemoved(true);
        tipRepository.save(tip);
    }

    @Override
    public Iterable<TipModel> getAllNotRemoved() {
        return tipRepository.getAllNotRemoved();
    }

    @Override
    public TipModel getTipById(final Long id) {
        return tipRepository.findById(id)
                .orElseThrow(() -> new TipNotFoundException(id));
    }
}
