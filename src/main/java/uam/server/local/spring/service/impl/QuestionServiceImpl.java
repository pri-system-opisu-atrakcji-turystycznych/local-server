package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.QuestionNotFoundException;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.repository.QuestionRepository;
import uam.server.local.spring.service.QuestionService;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Resource
    private QuestionRepository questionRepository;

    @Override
    public Iterable<QuestionModel> getAll() {
        return questionRepository.findAll();
    }

    @Override
    public QuestionModel save(final QuestionModel questionModel) {
        questionModel.setRemoved(false);
        return questionRepository.save(questionModel);
    }

    @Override
    public void delete(QuestionModel model) {
        questionRepository.delete(model);
    }

    @Override
    public void deleteById(Long id) {
        Optional<QuestionModel> message = Optional.ofNullable(getQuestionById(id));
        message.get().setRemoved(true);
        questionRepository.save(message.get());
    }

    @Override
    public Iterable<QuestionModel> getAllQuestionsWithoutElement() {
        return questionRepository.getAllWithoutElement();
    }

    @Override
    public QuestionModel getQuestionById(final Long id) {
        return questionRepository.findTopById(id)
                .orElseThrow(() -> new QuestionNotFoundException(id));
    }

}
