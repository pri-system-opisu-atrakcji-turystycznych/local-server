package uam.server.local.spring.service;

import uam.server.local.model.QuestionModel;

public interface QuestionService extends Service<QuestionModel> {

    Iterable<QuestionModel> getAll();

    Iterable<QuestionModel> getAllQuestionsWithoutElement();

    QuestionModel getQuestionById(final Long id);

    QuestionModel save(QuestionModel questionModel);

    void delete(QuestionModel model);

    void deleteById(Long id);
}
