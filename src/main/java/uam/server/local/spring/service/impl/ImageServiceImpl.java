package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.exception.ImageNotFoundException;
import uam.server.local.model.ImageModel;
import uam.server.local.spring.repository.ImageRepository;
import uam.server.local.spring.service.ImageService;

import javax.annotation.Resource;

@Service
public class ImageServiceImpl implements ImageService {

    @Resource
    private ImageRepository imageRepository;

    @Override
    public ImageModel getImageById(final Long id) {
        return imageRepository.findTopById(id)
                .orElseThrow(() -> new ImageNotFoundException(id));
    }

    @Override
    public Iterable<ImageModel> getAll() {
        return imageRepository.findAll();
    }

    @Override
    public ImageModel save(final ImageModel image) throws Exception {
        return imageRepository.save(image);
    }
}
