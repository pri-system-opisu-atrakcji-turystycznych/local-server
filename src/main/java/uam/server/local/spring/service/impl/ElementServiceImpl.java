package uam.server.local.spring.service.impl;

import org.springframework.stereotype.Service;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;
import uam.server.local.model.TipModel;
import uam.server.local.spring.repository.ElementRepository;
import uam.server.local.spring.service.ElementService;

import javax.annotation.Resource;

@Service
public class ElementServiceImpl implements ElementService {

    @Resource
    private ElementRepository elementRepository;

    @Override
    public Iterable<ElementModel> getAll() {
        return elementRepository.findAll();
    }

    @Override
    public ElementModel getElementById(final Long id) {
        return elementRepository.findById(id).get();
    }

    @Override
    public ElementModel getElementFromDescription(DescriptionModel model) {
        return getElementById(model.getElement().getId());
    }

    @Override
    public void delete(ElementModel model) {
        elementRepository.delete(model);
    }

    @Override
    public void clearTip(TipModel tip) {
        Iterable<ElementModel> elementWithTip = elementRepository.findByTip(tip);
        elementWithTip.forEach(e -> e.setTip(null));
        elementRepository.saveAll(elementWithTip);
    }

    @Override
    public Iterable<ElementModel> getAllNotRemoved() {
        return elementRepository.getAllNotRemoved();
    }

    @Override
    public void deleteById(Long id) {
        ElementModel model = getElementById(id);
        model.setRemoved(true);
        model.setTip(null);
        model.setQuestion(null);
        elementRepository.save(model);
    }

    @Override
    public ElementModel save(final ElementModel elementModel) throws Exception {
        return elementRepository.save(elementModel);
    }
}
