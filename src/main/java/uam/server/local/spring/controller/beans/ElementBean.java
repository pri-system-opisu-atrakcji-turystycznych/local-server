package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.model.ElementModel;
import uam.server.local.model.ImageModel;
import uam.server.local.model.QuestionModel;
import uam.server.local.model.TipModel;

import java.util.ArrayList;
@Component
public class ElementBean implements Bean<ElementModel> {
    private Iterable<QuestionModel> questionsWithoutElement;
    private ElementModel elementModelToAdd;
    private Iterable<ImageModel> images;
    private Iterable<TipModel> tips;
    private Iterable<ElementModel> elements;

    ElementBean() {
        elements = new ArrayList<>();
        tips = new ArrayList<>();
        images = new ArrayList<>();
        elementModelToAdd = new ElementModel();
        questionsWithoutElement = new ArrayList<>();
    }

    @Override
    public void clear() {
        elementModelToAdd = new ElementModel();
    }

    @Override
    public void prepareData(ElementModel elementModel) {

    }

    @Override
    public void setAfterSaveParam(ElementModel elementModel) {

    }

    @Override
    public ElementModel getElementModelToAdd() {
        return elementModelToAdd;
    }

    public void setQuestionsWithoutElement(Iterable<QuestionModel> allWithoutElement) {
        this.questionsWithoutElement = allWithoutElement;
    }

    public void setImages(Iterable<ImageModel> all) {
        this.images = all;
    }

    public void setTip(Iterable<TipModel> all) {
        this.tips = all;
    }

    public void setElements(Iterable<ElementModel> all) {
        this.elements = all;
    }

    public Iterable<ElementModel> getElements() {
        return elements;
    }

    public Iterable<QuestionModel> getQuestionsWithoutElement() {
        return questionsWithoutElement;
    }

    public Iterable<TipModel> getTips() {
        return tips;
    }

    public Iterable<ImageModel> getImages() {
        return images;
    }

    public void setElementIsEdit(boolean b) {
    }

    public void setElementIsDelete(boolean b) {
    }
}
