package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.TipData;
import uam.server.local.modalData.TipModalData;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.TipBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.facade.TipFacade;
import uam.server.local.spring.service.ImageService;
import uam.server.local.spring.service.TipService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(TipController.TipBeanAttributeName)
@RequestMapping("/tips")
public class TipController {
    static final String TipBeanAttributeName = ConstantsWithSuffix.TIP;

    @Resource
    private TipBean tipBean;
    @Resource
    private TipService tipService;
    @Resource
    private ImageService imageService;

    @Resource
    private TipFacade tipFacade;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<TipData> getAllTips() {
        return tipFacade.getAllTips();
    }

    @GetMapping(value = "/addTip")
    public ModelAndView addNewTip() {
        tipBean.clear();
        //do refaktoru
        tipBean.setImages(imageService.getAll());
        tipBean.setTips(tipFacade.getAllTips());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(TipBeanAttributeName, tipBean);
        modelAndView.addObject("elementToAdd", new TipModalData());
        modelAndView.setViewName(ViewNameConstants.TIP);
        return modelAndView;
    }

    @PostMapping(value = "/addTip")
    public ModelAndView createNewTip(@ModelAttribute final TipModalData tipData,
                                     final BindingResult bindingResult) {
        try {
            tipFacade.addTip(tipData);
        } catch (Exception e){
            tipBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd",  new TipModalData());
        modelAndView.addObject(TipBeanAttributeName, tipBean);
        modelAndView.setViewName(ViewNameConstants.TIP);
        return modelAndView;
    }

    @PutMapping("editTip")
    public ModelAndView updateTip(@ModelAttribute final TipModalData tipData,
                                  final BindingResult bindingResult){//TODO
        try {
            tipFacade.editTip(tipData);
        } catch (Exception e){
            tipBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd",  new TipModalData());
        modelAndView.addObject(TipBeanAttributeName, tipBean);
        modelAndView.setViewName(ViewNameConstants.TIP);
        return modelAndView;
    }

    @RequestMapping("/editTip/{id}")
    public ModelAndView prepareEditModal(@PathVariable final Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", tipFacade.getTipModalById(id));
        modelAndView.setViewName("htmlFragments/modals/tipModals :: updateModalTip");
        return modelAndView;
    }

    @RequestMapping("/deleteTip/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/tipModals :: deleteModalTip");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteTipConfirmed")
    public ModelAndView deleteTip(@ModelAttribute IdentityObject identityObject){
        try {
            tipFacade.deleteTip(identityObject.getIdentity());
        } catch (Exception e){
            tipBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new TipModalData());
        modelAndView.addObject(TipBeanAttributeName, tipBean);
        modelAndView.setViewName(ViewNameConstants.TIP);
        return modelAndView;
    }



    @ModelAttribute(TipBeanAttributeName)
    public Bean tipBean() {
        return new BeanFactory().getBean(BeanName.TIP);
    }
}
