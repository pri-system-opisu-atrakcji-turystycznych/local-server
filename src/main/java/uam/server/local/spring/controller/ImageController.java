package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uam.server.local.spring.facade.ImageFacade;
import uam.server.local.spring.service.ImageService;

import javax.annotation.Resource;
import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/image")
public class ImageController {

    @Resource
    private ImageFacade imageFacade;

    @Resource
    private ImageService imageService;

    @GetMapping(value = "/demo", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getDemoImage() throws IOException {

        return imageService.getImageById(1l).getPhoto();
    }

    @GetMapping(value = "/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImage(@PathVariable final Long id) throws IOException {

        return imageService.getImageById(id).getPhoto();
    }

}
