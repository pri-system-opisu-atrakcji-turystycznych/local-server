package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.RouteData;
import uam.server.local.modalData.RouteModalData;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.RouteBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.RouteFacade;
import uam.server.local.spring.service.RouteService;

import javax.annotation.Resource;
import javax.validation.Valid;

@CrossOrigin
@RestController
@SessionAttributes(RouteController.beanAttributeName)
@RequestMapping("/routes")
public class RouteController {

    @Resource
    private RouteFacade routeFacade;
    static final String beanAttributeName = ConstantsWithSuffix.ROUTE;
    @Resource
    private RouteService routeService;

    @Resource
    private RouteBean routeBean;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<RouteData> getAllRoutes() {
        return routeFacade.getAllRoutes();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RouteModel getRoute(@PathVariable final Long id) {
        return routeService.getRouteById(id);
    }

    @PostMapping(value = "/addRoute")
    public ModelAndView createNewRoute(@Valid @ModelAttribute final RouteModalData routeData, final BindingResult bindingResult) {
        try {
            routeFacade.addRoute(routeData);
        } catch (Exception e) {
            routeBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new RouteModalData());
        modelAndView.addObject(beanAttributeName, routeBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE);
        return modelAndView;
    }

    @GetMapping(value = "/addRoute")
    public ModelAndView addNewRoute() {
        routeBean.clear();
        //do refaktoru
        routeBean.setRoutes(routeFacade.getAllRoutes());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(beanAttributeName, routeBean);
        modelAndView.addObject(AddingMethodTemplate.ELEMENT_TO_ADD, new RouteModalData());
        modelAndView.setViewName(ViewNameConstants.ROUTE);
        return modelAndView;

    }

    @RequestMapping("/editRoute/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", routeFacade.getModalRouteById(id));
        modelAndView.setViewName("htmlFragments/modals/routeModals :: updateModalRoute");
        return modelAndView;
    }

    @PutMapping("editRoute")
    public ModelAndView updateMessage(@ModelAttribute final RouteModalData routeData,
                                      final BindingResult bindingResult) {//TODO
        try {
            routeFacade.editRoute(routeData);
        } catch (Exception e) {
            routeBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new RouteModalData());
        modelAndView.addObject(beanAttributeName, routeBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE);
        return modelAndView;
    }




    @RequestMapping("/deleteRoute/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/routeModals :: deleteModalRoute");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteRouteConfirmed")
    public ModelAndView deleteRoute(@ModelAttribute IdentityObject identityObject){
        try {
            routeFacade.deleteRoute(identityObject.getIdentity());
        } catch (Exception e) {
            routeBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new RouteModalData());
        modelAndView.addObject(beanAttributeName, routeBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE);
        return modelAndView;
    }

    @ModelAttribute(beanAttributeName)
    public Bean routeBean() {
        return new BeanFactory().getBean(BeanName.ROUTE);
    }
}
