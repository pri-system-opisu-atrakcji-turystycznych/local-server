package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.DescriptionData;
import uam.server.local.model.DescriptionModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.DescriptionBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.DescriptionFacade;
import uam.server.local.spring.service.DescriptionService;
import uam.server.local.spring.service.ElementService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(DescriptionController.descriptionBeanAttributeName)
@RequestMapping("/descriptions")
public class DescriptionController {

    static final String descriptionBeanAttributeName = ConstantsWithSuffix.DESCRIPTION;

    @Resource
    private DescriptionBean descriptionBean;

    @Resource
    private DescriptionFacade descriptionFacade;

    @Resource
    private DescriptionService descriptionService;
    @Resource
    private ElementService elementService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<DescriptionData> getAllDescriptions() {
        return descriptionFacade.getAllDescriptions();
    }

    @GetMapping(value = "element/{id}/mobile/{mobile}/web/{web}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DescriptionModel getDescription(@PathVariable final Long id,
                                           @PathVariable final boolean mobile,
                                           @PathVariable final boolean web) {
        if (mobile && web) {
            return descriptionService.getDescriptionByElementIdAndWebIsTrueAndMobileIsTrue(id);
        } else if (mobile) {
            return descriptionService.getDescriptionByElementIdAndMobileIsTrue(id);
        } else if (web) {
            return descriptionService.getDescriptionByElementIdAndWebIsTrue(id);
        } else {
            return descriptionService.getDescriptionByElementIdAndWebIsFalseAndMobileIsFalse(id);
        }
    }

    @GetMapping(value = "element/addDescription")
    public ModelAndView addNewDescription(@ModelAttribute(descriptionBeanAttributeName) final DescriptionBean descriptionBean) {
        descriptionBean.clear();
        descriptionBean.setElements(elementService.getAll());
        descriptionBean.setDescriptions(descriptionFacade.getAllDescriptions());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(descriptionBeanAttributeName, descriptionBean);
        modelAndView.addObject("elementToAdd", descriptionBean.getElementModelToAdd());
        modelAndView.setViewName(ViewNameConstants.DESCRIPTION);

        return modelAndView;
    }

    @PostMapping(value = "element/addDescription")
    public ModelAndView createNewDescription(
            @ModelAttribute final DescriptionModel descriptionModel,
            final BindingResult bindingResult,
            @ModelAttribute(descriptionBeanAttributeName) final DescriptionBean descriptionBean) {
        descriptionModel.setElement(elementService.getElementById(descriptionModel.getElement().getId()));
        AddingMethodTemplate<DescriptionBean> method = new AddingMethodTemplate<DescriptionBean>() {
            @Override
            public void serviceGetElements(final DescriptionBean bean) {
                bean.setDescriptions(descriptionFacade.getAllDescriptions());
                bean.setElements(elementService.getAll());
            }

            //TODO
            //do preapre data trzeba dodać łapanie rzeczy nie dodać więcej jak po jednym opisie webowym i mobilnym i jakims tam jeszcze

        };
        return method.addElementAndGetView(descriptionBean, bindingResult, descriptionBeanAttributeName, ViewNameConstants.DESCRIPTION, descriptionModel, descriptionService);
    }


    @RequestMapping("/editDescription/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", descriptionFacade.getDescriptionById(id));
        modelAndView.setViewName("htmlFragments/modals/descriptionModals :: updateModalDescription");
        return modelAndView;
    }

    @PutMapping("editDescription")
    public ModelAndView updateDescription(@ModelAttribute final DescriptionData descriptionData,
                                          final BindingResult bindingResult) {//TODO
        try {
            descriptionFacade.editDescription(descriptionData);
        } catch (Exception e) {
            descriptionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(descriptionBeanAttributeName, descriptionBean);
        modelAndView.addObject("elementToAdd", descriptionBean.getElementModelToAdd());
        modelAndView.setViewName(ViewNameConstants.DESCRIPTION);
        return modelAndView;
    }

    @RequestMapping("/deleteDescription/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/descriptionModals :: deleteModalDescription");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteDescriptionConfirmed")
    public ModelAndView deleteTip(@ModelAttribute IdentityObject identityObject){
        try {
            descriptionFacade.deleteDescription(identityObject.getIdentity());
        } catch (Exception e) {
            descriptionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(descriptionBeanAttributeName, descriptionBean);
        modelAndView.addObject("elementToAdd", descriptionBean.getElementModelToAdd());
        modelAndView.setViewName(ViewNameConstants.DESCRIPTION);
        return modelAndView;
    }


    @ModelAttribute(descriptionBeanAttributeName)
    public Bean descriptionBean() {
        return new BeanFactory().getBean(BeanName.DESCRIPTION);
    }

}


