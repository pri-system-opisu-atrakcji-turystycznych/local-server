package uam.server.local.spring.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uam.server.local.spring.facade.SoundFacade;
import uam.server.local.spring.service.SoundService;

import javax.annotation.Resource;
import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/sound")
public class SoundController {

    @Resource
    private SoundService soundService;

    @Resource
    private SoundFacade soundFacade;

    @GetMapping(value = "/demo", produces = "audio/mpeg")
    public byte[] getDemoSound() throws IOException {
        return soundService.getSoundById(1L).getSound();
    }

    @GetMapping(value = "/{id}", produces = "audio/mpeg")
    public byte[] getSound(@PathVariable final Long id) throws IOException {
        return soundService.getSoundById(id).getSound();
    }
}
