package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.RouteData;
import uam.server.local.model.RouteModel;

@Component
public class RouteBean implements Bean<RouteModel> {
    private RouteModel routeModel;
    private String newRouteName;
    private boolean isAdded;
    private Iterable<RouteData> routes;

    RouteBean() {
        routeModel = new RouteModel();
        isAdded = false;
        newRouteName = "";
    }

    public void setAfterSaveParam(RouteModel routeModel) {
        setIsAdded(true);
        newRouteName = routeModel.getName();
        this.routeModel = new RouteModel();

    }

    @Override
    public RouteModel getElementModelToAdd() {
        return routeModel;
    }

    public void setIsAdded(boolean b) {
        this.isAdded = b;
    }

    @Override
    public void clear() {
        this.isAdded = false;
        this.newRouteName = "";
        this.routeModel = new RouteModel();
    }

    @Override
    public void prepareData(RouteModel routeModel) throws NullPointerException {
        if (routeModel.getDescription() == null) {
            routeModel.setDescription("");
        }
        if (routeModel.getName() == null || routeModel.getName().equals("")) {
            throw new NullPointerException();
        }
        /* TODO */

    }

    public RouteModel getRouteModel() {
        return routeModel;
    }

    public Iterable<RouteData> getRoutes() {
        return routes;
    }

    public void setRoutes(Iterable<RouteData> routes) {
        this.routes = routes;
    }

    public void setIsRouteEdit(boolean b) {

    }

    public void setIsRouteDelete(boolean b) {

    }
}
