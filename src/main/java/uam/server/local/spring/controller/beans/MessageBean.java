package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.MessageData;
import uam.server.local.model.MessageModel;

import java.util.ArrayList;

@Component
public class MessageBean implements Bean<MessageModel> {
    private MessageModel elementToAdd;
    private Iterable<MessageData> messages;

    MessageBean() {
        elementToAdd = new MessageModel();
        messages = new ArrayList<>();
    }

    @Override
    public void clear() {
        elementToAdd = new MessageModel();
    }

    @Override
    public void prepareData(MessageModel messageModel) {

    }

    @Override
    public void setAfterSaveParam(MessageModel messageModel) {

    }

    @Override
    public MessageModel getElementModelToAdd() {
        return elementToAdd;
    }

    public void setMessages(Iterable<MessageData> all) {
        this.messages = all;
    }

    public void setElementToAdd(MessageModel messageModel) {
        elementToAdd = messageModel;
    }

    public Iterable<MessageData> getMessages() {
        return messages;
    }

    public void setIsMessageDelete(boolean b) {

    }

    public void setIsMessageEdit(boolean b) {

    }
}
