package uam.server.local.spring.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.model.UserModel;
import uam.server.local.spring.controller.beans.RegisterBean;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.facade.LoginFacade;
import uam.server.local.spring.service.impl.UserService;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.ValidationException;

@Controller
public class LoginController {

    @Resource
    private RegisterBean registerBean;
    @Resource
    private LoginFacade loginFacade;
    @Resource
    private UserService userService;

    @GetMapping(value = "/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(ViewNameConstants.LOGIN);
        return modelAndView;
    }

    @GetMapping(value = "/registration")
    public ModelAndView registration() {
        registerBean.clear();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(("registerBean"), registerBean);
        modelAndView.addObject("user", registerBean.getUser());
        modelAndView.setViewName(ViewNameConstants.REGISTRATION);
        return modelAndView;
    }

    @PostMapping(value = "/registration")
    public ModelAndView createNewUser(@Valid @ModelAttribute final UserModel user,
                                      final BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            loginFacade.register(user);

        } catch (IllegalArgumentException e) {
            bindingResult
                    .rejectValue("email", "errorUserEmail",
                            "There is already a user registered with the email provided");
            registerBean.clear();
        } catch (NullPointerException e) {
            bindingResult
                    .rejectValue("password", "errorUserPassword",
                            "Password is empty");
        } catch (ValidationException e) {
            bindingResult.rejectValue("password", "passwordValidationError", "Something wrong with your password, please check again.");
        }
        modelAndView.addObject("user", registerBean.getUser());
        modelAndView.addObject(("registerBean"), registerBean);
        modelAndView.setViewName(ViewNameConstants.REGISTRATION);
        return modelAndView;
    }

    @GetMapping(value = {"/", "/user/home"})
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserModel user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("userName", user.getEmail());
        modelAndView.addObject("adminRole", user.getRoles());
        modelAndView.setViewName(ViewNameConstants.HOME);
        return modelAndView;
    }
}
