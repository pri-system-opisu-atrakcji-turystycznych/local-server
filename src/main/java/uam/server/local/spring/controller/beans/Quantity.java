package uam.server.local.spring.controller.beans;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Quantity {
    private int quantity;

    public int getQuantity() {
        return quantity;
    }
}
