package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.model.QuestionModel;

import java.util.ArrayList;
@Component
public class QuestionBean implements Bean<QuestionModel> {
    private Iterable<QuestionData> questions;
    private QuestionModel elementToAdd;

    QuestionBean() {
        elementToAdd = new QuestionModel();
        questions = new ArrayList<>();
    }

    @Override
    public void clear() {
        elementToAdd = new QuestionModel();
    }

    @Override
    public void prepareData(QuestionModel questionModel) {

    }

    @Override
    public void setAfterSaveParam(QuestionModel questionModel) {

    }

    @Override
    public QuestionModel getElementModelToAdd() {
        return elementToAdd;
    }

    public Iterable<QuestionData> getQuestions() {
        return questions;
    }

    public void setQuestions(Iterable<QuestionData> questions) {
        this.questions = questions;
    }

    public QuestionModel elementToAdd() {
        return elementToAdd;
    }

    public void setElementToAdd(QuestionModel elementToAdd) {
        this.elementToAdd = elementToAdd;
    }

    public void setIsQuesitonEdit(boolean b) {

    }

    public void setIsQuestionDelete(boolean b) {

    }
}
