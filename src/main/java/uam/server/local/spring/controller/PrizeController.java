package uam.server.local.spring.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.PrizeData;
import uam.server.local.spring.controller.beans.*;
import uam.server.local.spring.controller.constants.BeansTypeConstants;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.facade.PrizeFacade;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping("/prize")
public class PrizeController {
    static final String prizeBeanAttributeName = BeansTypeConstants.ConstantsWithSuffix.PRIZE;
    @Resource
    private PrizeBean prizeBean;
    @Resource
    private PrizeFacade prizeFacade;

    @GetMapping(value = "")
    public PrizeData getPrize() {
        return prizeFacade.getPrize();
    }

    @PostMapping(value = "")
    public void addPrize(@ModelAttribute final int quantity) {
        prizeFacade.addPrizes(quantity);
    }


    @GetMapping(value = "/addPrizes")
    public ModelAndView addNewPrize() {
        prizeBean.clear();
        prizeFacade.prepareDefaultView();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(prizeBeanAttributeName, prizeBean);
        modelAndView.addObject("elementToAdd", prizeBean.getNumberOfAddPrizes());
        modelAndView.setViewName(ViewNameConstants.PRIZE);
        return modelAndView;
    }

    @PostMapping(value = "/addPrizes")
    public ModelAndView createNewPrizes(@ModelAttribute final Quantity quantity,
                                             final BindingResult bindingResult) {
        try {
            prizeFacade.addPrizes(quantity.getQuantity());
        } catch (Exception e){
            prizeBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", prizeBean.getNumberOfAddPrizes());
        modelAndView.addObject(prizeBeanAttributeName, prizeBean);
        modelAndView.setViewName(ViewNameConstants.PRIZE);
        return modelAndView;
    }


    @RequestMapping("/deletePrize/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/prizeModals :: deleteModalPrize");
        return modelAndView;
    }

    @DeleteMapping(value = "/deletePrizeConfirmed")
    public ModelAndView deleteRoute(@ModelAttribute IdentityObject identityObject){
        try {
            prizeFacade.deletePrize(identityObject.getIdentity());
        } catch (Exception e){
            prizeBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", prizeBean.getNumberOfAddPrizes());
        modelAndView.addObject(prizeBeanAttributeName, prizeBean);
        modelAndView.setViewName(ViewNameConstants.PRIZE);
        return modelAndView;
    }


}
