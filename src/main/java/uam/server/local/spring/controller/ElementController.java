package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.ElementData;
import uam.server.local.modalData.ElementModalData;
import uam.server.local.model.ElementModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.ElementBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.ElementFacade;
import uam.server.local.spring.service.ElementService;
import uam.server.local.spring.service.ImageService;
import uam.server.local.spring.service.QuestionService;
import uam.server.local.spring.service.TipService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(ElementController.elementBeanAttributeName)
@RequestMapping("/elements")
public class ElementController {
    @Resource
    private ElementBean elementBean;

    @Resource
    private ElementFacade elementFacade;

    static final String elementBeanAttributeName = ConstantsWithSuffix.ELEMENT;
    @Resource
    private TipService tipService;
    @Resource
    private QuestionService questionService;
    @Resource
    private ElementService elementService;
    @Resource
    private ImageService imageService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<ElementModel> getAllElements() {
        return elementService.getAllNotRemoved();
    }

    @GetMapping(value = "addElement")
    public ModelAndView addNewElement(@ModelAttribute(elementBeanAttributeName) final ElementBean elementBean) {
        elementBean.clear();
        elementBean.setQuestionsWithoutElement(questionService.getAllQuestionsWithoutElement());
        elementBean.setImages(imageService.getAll());
        elementBean.setTip(tipService.getAll());
        elementBean.setElements(elementService.getAll());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(elementBeanAttributeName, elementBean);
        modelAndView.addObject("elementToAdd", new ElementModalData());
        modelAndView.setViewName(ViewNameConstants.ELEMENT);

        return modelAndView;
    }

    @PostMapping(value = "addElement")
    public ModelAndView createNewElement(
                                         @ModelAttribute final ElementModalData elementModel,final BindingResult bindingResult) throws Exception {

        elementFacade.addElement(elementModel);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(elementBeanAttributeName, elementBean);
        modelAndView.addObject("elementToAdd", elementBean.getElementModelToAdd());
        modelAndView.setViewName(ViewNameConstants.ELEMENT);
        return modelAndView;
    }

    @PutMapping("editElement")
    public ModelAndView updateElement(@ModelAttribute final ElementModalData elementData,
                                  final BindingResult bindingResult){//TODO
        try {
            elementFacade.editElement(elementData);
        } catch (Exception e){
            elementBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new ElementModalData());
        modelAndView.addObject(elementBeanAttributeName, elementBean);
        modelAndView.setViewName(ViewNameConstants.ELEMENT);
        return modelAndView;
    }

    @RequestMapping("/editElement/{id}")
    public ModelAndView prepareEditModal(@PathVariable final Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", elementFacade.getElementModalById(id));
        modelAndView.setViewName("htmlFragments/modals/elementModals :: updateModalElement");
        return modelAndView;
    }

    @RequestMapping("/deleteElement/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/elementModals :: deleteModalElement");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteElementConfirmed")
    public ModelAndView deleteTip(@ModelAttribute IdentityObject identityObject){
        try {
            elementFacade.deleteElement(identityObject.getIdentity());
        } catch (Exception e){
            elementBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", new ElementModalData());
        modelAndView.addObject(elementBeanAttributeName, elementBean);
        modelAndView.setViewName(ViewNameConstants.ELEMENT);
        return modelAndView;
    }

    @ModelAttribute(elementBeanAttributeName)
    public Bean elementBean() {
        return new BeanFactory().getBean(BeanName.ELEMENT);
    }

}
