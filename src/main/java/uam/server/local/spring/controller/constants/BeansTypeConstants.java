package uam.server.local.spring.controller.constants;

public interface BeansTypeConstants {

    interface BeanName {
        String ROUTE = "route";
        String DESCRIPTION = "description";
        String ELEMENT = "element";
        String INSTITUTION = "institution";
        String MESSAGE = "prepareUpdateModal";
        String QUESTION = "questions";
        String ROUTE_ELEMENT = "routeElement";
        String TIP = "tip";
        String REGISTRATION = "registration";
        String PRIZE = "prize";
    }

    interface ConstantsWithSuffix {
        String REGISTRATION = "registerBean";
        String ROUTE = "routeBean";
        String DESCRIPTION = "descriptionBean";
        String ELEMENT = "elementBean";
        String INSTITUTION = "institutionBean";
        String MESSAGE = "messageBean";
        String QUESTION = "questionBean";
        String ROUTE_ELEMENT = "routeElementBean";
        String TIP = "tipBean";
        String PRIZE = "prizeBean";
    }

}