package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.PrizeData;

import java.util.ArrayList;
@Component
public class PrizeBean implements Bean<PrizeData> {
    private Iterable<PrizeData> prizes;
    private PrizeData prizeData;

    PrizeBean() {
        prizes = new ArrayList<>();
        prizeData = new PrizeData();
    }

    @Override
    public void clear() {
        prizeData = new PrizeData();
    }

    @Override
    public void prepareData(PrizeData prizeData) throws Exception {

    }

    @Override
    public void setAfterSaveParam(PrizeData prizeData) {

    }

    @Override
    public PrizeData getElementModelToAdd() {
        return prizeData;
    }

    public Quantity getNumberOfAddPrizes(){
        return new Quantity();
    }

    public Iterable<PrizeData> getPrizes() {
        return prizes;
    }

    public void setPrizes(Iterable<PrizeData> prizes) {
        this.prizes = prizes;
    }

    public PrizeData getPrizeData() {
        return prizeData;
    }

    public void setPrizeData(PrizeData prizeData) {
        this.prizeData = prizeData;
    }
}
