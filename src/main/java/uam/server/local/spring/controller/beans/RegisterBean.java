package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.model.RoleModel;
import uam.server.local.model.UserModel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class RegisterBean implements Bean<UserModel> {
    private static final String DEFAULT_ROLE = "REDAKTOR";
    private boolean isAdded;
    private UserModel user;
    private String newUserName;
    private RoleModel role;

    public RegisterBean() {
        role = new RoleModel();
        isAdded = false;
        user = new UserModel();
        newUserName = "";
    }

    public UserModel getUser() {
        return user;
    }

    public String getNewUserName() {
        return newUserName;
    }

    public void setAfterSaveParam(UserModel user) {
        setIsAdded(true);
        newUserName = user.getEmail();
        this.user = new UserModel();
        this.role = new RoleModel();

    }

    @Override
    public UserModel getElementModelToAdd() {
        return null;
    }

    public void setIsAdded(boolean b) {
        this.isAdded = b;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void clear() {
        this.isAdded = false;
        this.newUserName = "";
        this.user = new UserModel();
        this.role = new RoleModel();
    }

    @Override
    public void prepareData(UserModel user) {
        Set<RoleModel> defainedRole = user.getRoles();
        if (defainedRole == null || defainedRole.isEmpty()) {
            setDefaultUserRoles();
        }
    }

    private void setDefaultUserRoles() {
        role.setRole(DEFAULT_ROLE);
        user.setRoles(new HashSet<RoleModel>(Arrays.asList(role)));

    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public void setLogedUser(UserModel user) {
    }

    public UserModel getLoggedUser() {
        return null;
    }
}
