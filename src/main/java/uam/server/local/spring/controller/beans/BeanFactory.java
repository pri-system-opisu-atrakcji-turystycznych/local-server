package uam.server.local.spring.controller.beans;

import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;

public class BeanFactory {
    public Bean getBean(String beanType) {
        if (beanType == null) {
            return null;
        }
        if (beanType.equals(BeanName.REGISTRATION)) {
            //return new RegisterBean();
        } else if (beanType.equals(BeanName.ROUTE)) {
            return new RouteBean();
        } else if (beanType.equals(BeanName.DESCRIPTION)) {
            return new DescriptionBean();
        } else if (beanType.equals(BeanName.ELEMENT)) {
            return new ElementBean();
        } else if (beanType.equals(BeanName.INSTITUTION)) {
            return new InstitutionBean();
        } else if (beanType.equals(BeanName.MESSAGE)) {
            return new MessageBean();
        } else if (beanType.equals(BeanName.QUESTION)) {
            return new QuestionBean();
        } else if (beanType.equals(BeanName.ROUTE_ELEMENT)) {
            return new RouteElementBean();
        } else if (beanType.equals(BeanName.TIP)) {
            return new TipBean();
        }
        return null;
    }
}