package uam.server.local.spring.controller.methodTemplate;

import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.service.Service;

import java.util.Locale;

public abstract class AddingMethodTemplate<BeanType extends Bean> {

    public static final String ELEMENT_TO_ADD = "elementToAdd";
    private MessageSource messageSource;

    public final ModelAndView addElementAndGetView(BeanType bean, BindingResult bindingResult, String beanAttributeName, String viewName, Object elementToAdd, Service service) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            bean.prepareData(elementToAdd);
            service.save(elementToAdd);
            serviceGetElements(bean);
        } catch (Exception e) {
//            bindingResult.reject(messageSource.getMessage("adding.error.message.text", null, Locale.getDefault()));
        }
        if (bindingResult.hasErrors()) {
            bean.clear();
        } else {
            bean.setAfterSaveParam(elementToAdd);
        }
        modelAndView.addObject(ELEMENT_TO_ADD, bean.getElementModelToAdd());
        modelAndView.addObject(beanAttributeName, bean);
        modelAndView.setViewName(viewName);

        return modelAndView;
    }

    public abstract void serviceGetElements(BeanType bean);
}
