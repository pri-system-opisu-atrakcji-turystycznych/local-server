package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.InstitutionData;
import uam.server.local.model.InstitutionModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.InstitutionBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.InstitutionFacade;
import uam.server.local.spring.service.InstitutionService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(InstitutionController.institutionBeanAttributeName)
@RequestMapping("/institutions")
public class InstitutionController {

    @Resource
    private InstitutionFacade institutionFacade;

    static final String institutionBeanAttributeName = ConstantsWithSuffix.INSTITUTION;

    @Resource
    private InstitutionService institutionService;

    @Resource
    private InstitutionBean institutionBean;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<InstitutionData> getAllInstitutions() {
        return institutionFacade.getAllInstitution();
    }

    @GetMapping(value = "/main", produces = MediaType.APPLICATION_JSON_VALUE)
    public InstitutionData getMainInstitution() {
        return institutionFacade.getInstitutionByNameIsNotNull();
    }

    @GetMapping(value = "/addInstitution")
    public ModelAndView addNewInstitution(@ModelAttribute(institutionBeanAttributeName) final InstitutionBean institutionBean) {
        institutionBean.clear();
        institutionBean.setInstitutions(institutionFacade.getAllInstitution());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(institutionBeanAttributeName, institutionBean);
        modelAndView.addObject("elementToAdd", institutionBean.getInstitutionModel());
        modelAndView.setViewName(ViewNameConstants.INSTITUTION);
        return modelAndView;
    }

    @PostMapping(value = "/addInstitution")
    public ModelAndView createNewInstitution(@ModelAttribute final InstitutionModel institutionModel,
                                             @ModelAttribute(institutionBeanAttributeName) final InstitutionBean institutionBean,
                                             final BindingResult bindingResult) {

        AddingMethodTemplate<InstitutionBean> method = new AddingMethodTemplate<InstitutionBean>() {
            @Override
            public void serviceGetElements(final InstitutionBean bean) {
                bean.setInstitutions(institutionFacade.getAllInstitution());
            }
        };

        return method.addElementAndGetView(institutionBean, bindingResult, institutionBeanAttributeName,
                ViewNameConstants.INSTITUTION, institutionModel, institutionService);
    }

    @RequestMapping("/editInstitution/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id){

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", institutionFacade.getMessageById(id));
        modelAndView.setViewName("htmlFragments/modals/institutionModals :: updateModalInstitution");
        return modelAndView;
    }

    @PutMapping("editInstitution")
    public ModelAndView updateInstitution(@ModelAttribute final InstitutionData institutionData,
                                      final BindingResult bindingResult){//TODO
        try {
            institutionFacade.editInstitution(institutionData);
        } catch (Exception e){
            institutionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", institutionBean.getElementModelToAdd());
        modelAndView.addObject(institutionBeanAttributeName, institutionBean);
        modelAndView.setViewName(ViewNameConstants.INSTITUTION);
        return modelAndView;
    }

    @RequestMapping("/deleteInstitution/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/institutionModals :: deleteModalInstitution");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteInstitutionConfirmed")
    public ModelAndView deleteInstitution(@ModelAttribute IdentityObject identityObject){
        try {
            institutionFacade.deleteInstitution(identityObject.getIdentity());
        } catch (Exception e){
            institutionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", institutionBean.getElementModelToAdd());
        modelAndView.addObject(institutionBeanAttributeName, institutionBean);
        modelAndView.setViewName(ViewNameConstants.INSTITUTION);
        return modelAndView;
    }


    @ModelAttribute(institutionBeanAttributeName)
    public Bean institutionBean() {
        return new BeanFactory().getBean(BeanName.INSTITUTION);
    }

}
