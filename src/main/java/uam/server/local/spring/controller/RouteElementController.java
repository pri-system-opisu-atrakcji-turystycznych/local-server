package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.RouteElementData;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.RouteElementBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.RouteElementFacade;
import uam.server.local.spring.service.ElementService;
import uam.server.local.spring.service.RouteElementService;
import uam.server.local.spring.service.RouteService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(RouteElementController.routeElementBeanAttributeName)
@RequestMapping("/routes-elements")
public class RouteElementController {
    static final String routeElementBeanAttributeName = ConstantsWithSuffix.ROUTE_ELEMENT;
    @Resource
    private RouteElementFacade routeElementFacade;

    @Resource
    private RouteService routeService;
    @Resource
    private RouteElementService routeElementService;
    @Resource
    private ElementService elementService;
    @Resource
    private RouteElementBean routeElementBean;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<RouteElementModel> getAllRoutesElements() {
        return routeElementService.getAllNotRemoved();
    }

    @GetMapping(value = "/route/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<RouteElementModel> getAllElements(@PathVariable final Long id) {
        return routeElementService.getRouteElementByRouteId(id);
    }

    @GetMapping(value = "/route/addRouteElement")
    public ModelAndView addNewRouteElement(@ModelAttribute(routeElementBeanAttributeName) final RouteElementBean routeElementBean) {
        ModelAndView modelAndView = new ModelAndView();
        routeElementBean.clear();
        routeElementBean.setAllElements(elementService.getAllNotRemoved());
        routeElementBean.setRoutes(routeService.getAllNotRemoved());
        routeElementBean.setRoutesElements(routeElementService.getAllNotRemoved());
        modelAndView.addObject("elementToAdd", routeElementBean.getElementModelToAdd());
        modelAndView.addObject(routeElementBeanAttributeName, routeElementBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE_ELEMENT);
        return modelAndView;
    }

    @PostMapping(value = "/route/{id}/addRouteElement")
    public ModelAndView createNewRouteElement(@PathVariable final Long id,
                                              @ModelAttribute final RouteElementModel routeElementModel,
                                              @ModelAttribute(routeElementBeanAttributeName) final RouteElementBean routeElementBean,
                                              final BindingResult bindingResult) {
        routeElementModel.setId(null);
        routeElementModel.setRoute(routeService.getRouteById(id));
        AddingMethodTemplate<RouteElementBean> method = new AddingMethodTemplate<RouteElementBean>() {
            @Override
            public void serviceGetElements(final RouteElementBean bean) {
                bean.setAllElements(elementService.getAllNotRemoved());
                bean.setRoutes(routeService.getAllNotRemoved());
                bean.setRoutesElements(routeElementService.getAllNotRemoved());
            }
        };

        return method.addElementAndGetView(routeElementBean, bindingResult, routeElementBeanAttributeName,
                ViewNameConstants.ROUTE_ELEMENT, routeElementModel, routeElementService);
    }

    @RequestMapping("/route/editRouteElement/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", routeElementFacade.getMessageById(id));
        modelAndView.setViewName("htmlFragments/modals/routeElementModals :: updateModalRouteElement");
        return modelAndView;
    }

    @PutMapping("/route/editRouteElement")
    public ModelAndView updateRouteElement(@ModelAttribute final RouteElementData routeElementData,
                                           final BindingResult bindingResult) {//TODO
        try {
            routeElementFacade.editRouteElement(routeElementData);
        } catch (Exception e) {
            routeElementBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", routeElementBean.getElementModelToAdd());
        modelAndView.addObject(routeElementBeanAttributeName, routeElementBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE_ELEMENT);
        return modelAndView;
    }

    @RequestMapping("/route/deleteRouteElement/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/routeElementModals :: deleteModalRouteElement");
        return modelAndView;
    }

    @DeleteMapping(value = "/route/deleteRouteElementConfirmed")
    public ModelAndView deleteRouteElement(@ModelAttribute IdentityObject identityObject){
        try {
            routeElementFacade.deleteRouteElement(identityObject.getIdentity());
        } catch (Exception e){
            routeElementBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", routeElementBean.getElementModelToAdd());
        modelAndView.addObject(routeElementBeanAttributeName, routeElementBean);
        modelAndView.setViewName(ViewNameConstants.ROUTE_ELEMENT);
        return modelAndView;
    }


    @ModelAttribute(routeElementBeanAttributeName)
    public Bean routeElementBean() {
        return new BeanFactory().getBean(BeanName.ROUTE_ELEMENT);
    }
}
