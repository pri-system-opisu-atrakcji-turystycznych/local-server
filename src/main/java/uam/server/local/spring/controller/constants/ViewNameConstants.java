package uam.server.local.spring.controller.constants;

public interface ViewNameConstants {

    String DESCRIPTION = "user/descriptionAdding";
    String ELEMENT = "user/elementAdding";
    String IMAGE = "user/imageAdding";
    String INSTITUTION = "user/institutionAdding";
    String MESSAGE = "user/messageAdding";
    String QUESTION = "user/questionAdding";
    String ROUTE = "user/routeAdding";
    String ROUTE_ELEMENT = "user/elementRouteAdding";
    String TIP = "user/tipAdding";
    String LOGIN = "login";
    String REGISTRATION = "registration";
    String HOME = "user/home";
    String PRIZE = "user/prizeAdding";

}
