package uam.server.local.spring.controller;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdentityObject {
    private Long identity;

    public Long getIdentity(){
        return identity;
    }
}
