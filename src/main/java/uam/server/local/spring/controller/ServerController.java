package uam.server.local.spring.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/server")
public class ServerController {

    @GetMapping(value = "/status")
    public String getStatus() {
        return "Work!";
    }

}
