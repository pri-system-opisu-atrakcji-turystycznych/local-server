package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.QuestionBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.QuestionFacade;
import uam.server.local.spring.service.QuestionService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(QuestionController.questionBeanAttributeName)
@RequestMapping("/questions")
public class QuestionController {
    static final String questionBeanAttributeName = ConstantsWithSuffix.QUESTION;

    @Resource
    private QuestionFacade questionFacade;

    @Resource
    private QuestionService questionService;
    @Resource
    QuestionBean questionBean;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<QuestionData> getAllQuestions() {
        return questionFacade.getAllQuestions();
    }

    @GetMapping(value = "/addQuestion")
    public ModelAndView addNewQuestion(@ModelAttribute(questionBeanAttributeName) final QuestionBean questionBean) {
        questionBean.clear();
        //do refaktoru
        questionBean.setQuestions(questionFacade.getAllQuestions());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(questionBeanAttributeName, questionBean);
        modelAndView.addObject("elementToAdd", questionBean.elementToAdd());
        modelAndView.setViewName(ViewNameConstants.QUESTION);
        return modelAndView;
    }

    @PostMapping(value = "/addQuestion")
    public ModelAndView createNewQuestion(@ModelAttribute(questionBeanAttributeName) final QuestionBean questionBean,
                                          @ModelAttribute final QuestionModel questionModel,
                                          final BindingResult bindingResult) {
        AddingMethodTemplate<QuestionBean> method = new AddingMethodTemplate<QuestionBean>() {
            @Override
            public void serviceGetElements(final QuestionBean bean) {
                bean.setQuestions(questionFacade.getAllQuestions());
            }
        };

        return method.addElementAndGetView(questionBean, bindingResult, questionBeanAttributeName,
                ViewNameConstants.QUESTION, questionModel, questionService);

    }

    @RequestMapping("/editQuestion/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", questionFacade.getQuestionById(id));
        modelAndView.setViewName("htmlFragments/modals/questionModals :: updateModalQuestion");
        return modelAndView;
    }

    @PutMapping("editQuestion")
    public ModelAndView updateQuestion(@ModelAttribute final QuestionData questionData,
                                       final BindingResult bindingResult) {//TODO
        try {
            questionFacade.editQuestion(questionData);
        } catch (Exception e) {
            questionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", questionBean.getElementModelToAdd());
        modelAndView.addObject(questionBeanAttributeName, questionBean);
        modelAndView.setViewName(ViewNameConstants.QUESTION);
        return modelAndView;
    }



    @RequestMapping("/deleteQuestion/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/questionModals :: deleteModalQuestion");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteQuestionConfirmed")
    public ModelAndView deleteRoute(@ModelAttribute IdentityObject identityObject){
        try {
            questionFacade.deleteQuestion(identityObject.getIdentity());
        } catch (Exception e) {
            questionBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", questionBean.getElementModelToAdd());
        modelAndView.addObject(questionBeanAttributeName, questionBean);
        modelAndView.setViewName(ViewNameConstants.QUESTION);
        return modelAndView;
    }

    @ModelAttribute(questionBeanAttributeName)
    public Bean questionBean() {
        return new BeanFactory().getBean(BeanName.QUESTION);
    }
}
