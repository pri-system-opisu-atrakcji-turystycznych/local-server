package uam.server.local.spring.controller.beans;

public interface Bean<modelType> {
    void clear();

    void prepareData(modelType modelType) throws Exception;

    void setAfterSaveParam(modelType modelType);

    modelType getElementModelToAdd();
}