package uam.server.local.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uam.server.local.data.impl.MessageData;
import uam.server.local.model.MessageModel;
import uam.server.local.spring.controller.beans.Bean;
import uam.server.local.spring.controller.beans.BeanFactory;
import uam.server.local.spring.controller.beans.MessageBean;
import uam.server.local.spring.controller.constants.BeansTypeConstants.BeanName;
import uam.server.local.spring.controller.constants.BeansTypeConstants.ConstantsWithSuffix;
import uam.server.local.spring.controller.constants.ViewNameConstants;
import uam.server.local.spring.controller.methodTemplate.AddingMethodTemplate;
import uam.server.local.spring.facade.MessageFacade;
import uam.server.local.spring.service.MessageService;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@SessionAttributes(MessageController.messageBeanAttributeName)
@RequestMapping("/messages")
public class MessageController {
    static final String messageBeanAttributeName = ConstantsWithSuffix.MESSAGE;

    @Resource
    private MessageFacade messageFacade;

    @Resource
    private MessageService messageService;

    @Resource
    private MessageBean messageBean;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<MessageData> getAllMessages() {
        return messageFacade.getAllMessages();
    }

    @GetMapping(value = "/addMessage")
    public ModelAndView addNewInstitution(@ModelAttribute(messageBeanAttributeName) final MessageBean messageBean) {
        messageBean.clear();
        messageBean.setMessages(messageFacade.getAllMessages());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(messageBeanAttributeName, messageBean);
        modelAndView.addObject("elementToAdd", messageBean.getElementModelToAdd());
        modelAndView.setViewName(ViewNameConstants.MESSAGE);
        return modelAndView;
    }

    @PostMapping(value = "/addMessage")
    public ModelAndView createNewInstitution(@ModelAttribute final MessageModel messageModel,
                                             @ModelAttribute(messageBeanAttributeName) final MessageBean messageBean,
                                             final BindingResult bindingResult) {
        AddingMethodTemplate<MessageBean> method = new AddingMethodTemplate<MessageBean>() {
            @Override
            public void serviceGetElements(final MessageBean bean) {
                bean.setMessages(messageFacade.getAllMessages());
            }
        };

        return method.addElementAndGetView(messageBean, bindingResult, messageBeanAttributeName,
                ViewNameConstants.MESSAGE, messageModel, messageService);
    }


    @RequestMapping("/editMessage/{id}")
    public ModelAndView prepareUpdateModal(@PathVariable final Long id){

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fieldToUpdate", messageFacade.getMessageById(id));
        modelAndView.setViewName("htmlFragments/modals/messageModals :: updateModalMessage");
        return modelAndView;
    }

    @PutMapping("editMessage")
    public ModelAndView updateMessage(@ModelAttribute final MessageData messageData,
                                  final BindingResult bindingResult){//TODO
        try {
            messageFacade.editMessage(messageData);
        } catch (Exception e){
            messageBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", messageBean.getElementModelToAdd());
        modelAndView.addObject(messageBeanAttributeName, messageBean);
        modelAndView.setViewName(ViewNameConstants.MESSAGE);
        return modelAndView;
    }

    @RequestMapping("/deleteMessage/{id}")
    public ModelAndView prepareDeleteModal(@PathVariable final Long id) {

        ModelAndView modelAndView = new ModelAndView();
        IdentityObject identityObject = new IdentityObject(id);
        modelAndView.addObject("identityObject", identityObject);
        modelAndView.setViewName("htmlFragments/modals/messageModals :: deleteModalMessage");
        return modelAndView;
    }

    @DeleteMapping(value = "/deleteMessageConfirmed")
    public ModelAndView deleteRoute(@ModelAttribute IdentityObject identityObject){
        try {
            messageFacade.deleteMessage(identityObject.getIdentity());
        } catch (Exception e){
            messageBean.clear();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("elementToAdd", messageBean.getElementModelToAdd());
        modelAndView.addObject(messageBeanAttributeName, messageBean);
        modelAndView.setViewName(ViewNameConstants.MESSAGE);
        return modelAndView;
    }


    @ModelAttribute(messageBeanAttributeName)
    public Bean messageBean() {
        return new BeanFactory().getBean(BeanName.MESSAGE);
    }
}
