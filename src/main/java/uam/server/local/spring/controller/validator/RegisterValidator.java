package uam.server.local.spring.controller.validator;

import org.springframework.stereotype.Component;
import uam.server.local.model.UserModel;

import javax.validation.ValidationException;

@Component
public class RegisterValidator {
    public void passwordValidate(UserModel user) {
        if (user.getPassword().isEmpty()) {
            throw new NullPointerException();
        }
        if (!isPasswordStrucValidate(user.getPassword())) {
            throw new ValidationException();
        }
    }

    private Boolean isPasswordStrucValidate(String password) {
        //TODO
        return true;
    }

    public void passwordChangingValidate(String newPassowrd, String oldPassword) {
    }
}
//TODO