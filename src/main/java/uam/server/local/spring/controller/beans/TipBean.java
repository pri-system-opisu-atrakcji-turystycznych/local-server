package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.TipData;
import uam.server.local.model.ImageModel;

import java.util.ArrayList;

/*import uam.server.local.sun.deploy.panel.ITreeNode;*/
@Component
public class TipBean implements Bean {
    private TipData tipData;
    private Iterable<TipData> tips;
    private Iterable<ImageModel> images;

    TipBean() {
        tipData = new TipData();
        tips = new ArrayList<>();
        images = new ArrayList<>();
    }

    @Override
    public void clear() {
        tipData = new TipData();
    }

    @Override
    public void prepareData(Object o) {

    }

    @Override
    public void setAfterSaveParam(Object o) {

    }

    @Override
    public Object getElementModelToAdd() {
        return tipData;
    }

    public void setTips(Iterable<TipData> tips) {
        this.tips = tips;
    }

    public TipData getTipModel() {
        return tipData;
    }

    public void setTipModel(TipData tipData) {
        this.tipData= tipData;
    }

    public Iterable<TipData> getTips() {
        return tips;
    }

    public void setImages(Iterable<ImageModel> images) {
        this.images = images;
    }

    public Iterable<ImageModel> getImages() {
        return images;
    }

    public void setTipIsChanged(boolean b) {
    }

    public void setTipIsDeleted(boolean b) {
    }
}
