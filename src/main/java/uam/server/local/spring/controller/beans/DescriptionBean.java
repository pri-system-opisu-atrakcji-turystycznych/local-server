package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.DescriptionData;
import uam.server.local.exception.DescriptionParameterNotFoundException;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;

import java.util.ArrayList;
import java.util.stream.StreamSupport;
@Component
public class DescriptionBean implements Bean<DescriptionModel> {
    private static final String NO_TITLE = "Brak tytułu";
    private static final String NO_CONETENT = "Brak opisu";
    private Iterable<ElementModel> elements;
    private Iterable<DescriptionData> descriptions;
    private DescriptionModel descriptionModel;

    DescriptionBean() {
        descriptions = new ArrayList<>();
        elements = new ArrayList<>();
        descriptionModel = new DescriptionModel();
    }

    @Override
    public void clear() {
        descriptionModel = new DescriptionModel();
    }

    @Override
    public void prepareData(DescriptionModel descriptionModel) throws Exception {
        if (descriptionModel.getElement() == null || descriptionModel.getElement().getId() == 0 || descriptionModel.getElement().getId() == null) {
            throw new DescriptionParameterNotFoundException(descriptionModel);
        }
        descriptionModel.setElement(StreamSupport.stream(elements.spliterator(), false)
                .filter(element -> descriptionModel.getElement().getId().equals(element.getId()))
                .findFirst().get());
        if (descriptionModel.getTitle() == null || descriptionModel.getTitle().equals("")) {
            descriptionModel.setTitle(NO_TITLE);
        }
        if (descriptionModel.getContent() == null || descriptionModel.getContent().equals("")) {
            descriptionModel.setContent(NO_CONETENT);
        }
        if (descriptionModel.getWeb() == null) {
            descriptionModel.setWeb(false);
        }
        if (descriptionModel.getMobile() == null) {
            descriptionModel.setMobile(false);
        }
    }

    @Override
    public void setAfterSaveParam(DescriptionModel description) {
    }

    @Override
    public DescriptionModel getElementModelToAdd() {
        return descriptionModel;
    }

    public void setElements(Iterable<ElementModel> elements) {
        this.elements = elements;
    }

    public Iterable<ElementModel> getElements() {
        return elements;
    }

    public Iterable<DescriptionData> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Iterable<DescriptionData> descriptions) {
        this.descriptions = descriptions;
    }

    public void setDescriptionIsDelete(boolean b) {
    }

    public void setDescriptionIsEdit(boolean b) {
    }
}
