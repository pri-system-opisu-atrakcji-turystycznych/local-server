package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.data.impl.InstitutionData;
import uam.server.local.model.InstitutionModel;

import java.util.ArrayList;
@Component
public class InstitutionBean implements Bean<InstitutionModel> {
    private Iterable<InstitutionData> institutions;
    private InstitutionModel institutionModel;

    InstitutionBean() {
        institutions = new ArrayList<>();
        institutionModel = new InstitutionModel();

    }

    @Override
    public void clear() {
        institutionModel = new InstitutionModel();
    }

    @Override
    public void prepareData(InstitutionModel institutionModel) {

    }

    @Override
    public void setAfterSaveParam(InstitutionModel institutionModel) {

    }

    @Override
    public InstitutionModel getElementModelToAdd() {
        return institutionModel;
    }

    public void setInstitutions(Iterable<InstitutionData> institutions) {
        this.institutions = institutions;
    }

    public InstitutionModel getInstitutionModel() {
        return institutionModel;
    }

    public Iterable<InstitutionData> getInstitutions() {
        return institutions;
    }

    public void setIsInstitutionDelete(boolean b) {
    }

    public void setIsInstitutionEdit(boolean b) {
    }
}
