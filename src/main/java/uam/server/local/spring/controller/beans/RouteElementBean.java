package uam.server.local.spring.controller.beans;

import org.springframework.stereotype.Component;
import uam.server.local.model.ElementModel;
import uam.server.local.model.RouteElementModel;
import uam.server.local.model.RouteModel;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
@Component
public class RouteElementBean implements Bean<RouteElementModel> {
    private RouteElementModel routeElementModel;
    private Iterable<RouteElementModel> routesElements;
    private Iterable<RouteModel> routes;
    private boolean isAdded;
    private Iterable<ElementModel> allElements;

    RouteElementBean() {
        isAdded = false;
        routesElements = new ArrayList<>();
        routes = new ArrayList<>();
        routeElementModel = new RouteElementModel();
        allElements = new ArrayList<>();
    }

    @Override
    public void clear() {
        setIsAdded(false);
        routeElementModel = new RouteElementModel();
    }

    public void setIsAdded(boolean b) {
        this.isAdded = b;
    }

    @Override
    public void prepareData(RouteElementModel o) {

    }

    @Override
    public void setAfterSaveParam(RouteElementModel o) {

    }

    @Override
    public RouteElementModel getElementModelToAdd() {
        return routeElementModel;
    }

    public void setRoutes(Iterable<RouteModel> routes) {
        this.routes = routes;
    }

    public void setRoutesElements(Iterable<RouteElementModel> routesElements) {
        this.routesElements = routesElements;
    }

    public Iterable<RouteElementModel> getRoutesElements() {
        return routesElements;

    }

    public Iterable<RouteElementModel> getElementsByRoute(RouteModel routeModel) {
        return StreamSupport.stream(routesElements.spliterator(), false)
                .filter(routeElement1 -> routeModel.equals(routeElement1.getRoute()))
                .collect(Collectors.toCollection(ArrayList::new));

    }

    public Iterable<RouteModel> getRoutes() {
        return routes;
    }

    public void setAllElements(Iterable<ElementModel> elements) {
        allElements = elements;
    }

    public Iterable<ElementModel> getAllElements() {
        return allElements;
    }

    public void setIsRouteElementDelete(boolean b) {

    }

    public void setIsRouteElementEdit(boolean b) {

    }
}
