package uam.server.local.spring.facade.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import uam.server.local.model.UserModel;
import uam.server.local.spring.controller.beans.RegisterBean;
import uam.server.local.spring.controller.validator.RegisterValidator;
import uam.server.local.spring.converter.impl.UserConverter;
import uam.server.local.spring.facade.LoginFacade;
import uam.server.local.spring.service.impl.UserService;

import javax.annotation.Resource;

@Service
public class LoginFacadeImpl implements LoginFacade {

    @Resource
    private RegisterValidator registerValidator;
    @Resource
    private RegisterBean registerBean;
    @Resource
    private UserService userService;
    @Resource
    private UserConverter userConverter;

    public void register(final UserModel user) {
        registerBean.clear();
        registerBean.setUser(user);
        if (userService.isExist(user.getEmail())) {
            throw new IllegalArgumentException();
        }
        registerValidator.passwordValidate(user);
        registerBean.prepareData(user);
        userService.saveUser(user);
        registerBean.setAfterSaveParam(user);
    }

    public void editOwnAccount(final UserModel newUserData) {
        String email = registerBean.getLoggedUser().getEmail();
        UserModel dataToChange = userService.findUserByEmail(email);
        if (userService.isExist(newUserData.getEmail())) {
            throw new IllegalArgumentException();
        }
        dataToChange.setEmail(newUserData.getEmail());
        registerValidator.passwordChangingValidate(newUserData.getPassword(), dataToChange.getPassword());
        dataToChange.setPassword(newUserData.getPassword());
        userService.updateUser(dataToChange);
    }

    public void editUserByAdmin(final String email, final UserModel dataToChange) {
        UserModel user = userService.findUserByEmail(email);
        if (userService.isExist(dataToChange.getEmail())) {
            throw new IllegalArgumentException();
        }
        user.setEmail(dataToChange.getEmail());
        user.setRoles(dataToChange.getRoles());
        userService.updateUserByAdmin(user);

    }

    public void deleteUser(final String email) {
        UserModel userToDelete = userService.findUserByEmail(email);
        userToDelete.setActive(0L);
        userService.updateUser(userToDelete);
    }

    public void logIn() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserModel user = userService.findUserByEmail(auth.getName());
        registerBean.setLogedUser(user);
    }

}
