package uam.server.local.spring.facade;

import uam.server.local.data.impl.RouteElementData;

public interface RouteElementFacade {
    void addRouteElement(RouteElementData routeElementData);

    void editRouteElement(RouteElementData routeElementData);

    void deleteRouteElement(long id);

    RouteElementData getMessageById(Long id);
}

