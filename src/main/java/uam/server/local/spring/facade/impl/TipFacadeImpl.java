package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.TipData;
import uam.server.local.modalData.TipModalData;
import uam.server.local.model.ImageModel;
import uam.server.local.model.TipModel;
import uam.server.local.spring.controller.beans.TipBean;
import uam.server.local.spring.converter.impl.TipConverter;
import uam.server.local.spring.converter.impl.TipModalConverter;
import uam.server.local.spring.facade.TipFacade;
import uam.server.local.spring.service.ImageService;
import uam.server.local.spring.service.TipService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;

@Service
public class TipFacadeImpl implements TipFacade {
    @Resource
    private TipBean tipBean;

    @Resource
    private TipService tipService;

    @Resource
    private TipConverter tipConverter;

    @Resource
    private ImageService imageService;

    @Resource
    private TipModalConverter tipModalConverter;

    //method name to change
    @Override
    public void addTip(TipModalData tipData) throws Exception {
        if (tipData.getPhoto() !=null && tipData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(tipData.getPhoto().getBytes());
            image = imageService.save(image);
            TipModel model = tipModalConverter.inverseConvert(tipData);
            model.setPhoto(image.getId());
            tipService.save(model);
        } else {
            TipModel model = tipModalConverter.inverseConvert(tipData);
            tipService.save(model);
        }
        tipBean.setTips(getAllTips());
        tipBean.setImages(imageService.getAll());
    }

    @Override
    public void editTip(TipModalData tipData) throws Exception {
        if (tipData.getPhoto() !=null && tipData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(tipData.getPhoto().getBytes());
            image = imageService.save(image);
            TipModel model = tipModalConverter.inverseConvert(tipData);
            model.setPhoto(image.getId());
            tipService.save(model);
        } else {
            ImageModel image = imageService.getImageById(tipService.getTipById(tipData.getId()).getPhoto());
            TipModel model = tipModalConverter.inverseConvert(tipData);
            model.setPhoto(image.getId());
            tipService.save(model);
        }
        tipBean.setTipIsChanged(true);
        tipBean.setTips(getAllTips());
        tipBean.setImages(imageService.getAll());
    }

    @Override
    public void deleteTip(Long id) {
        tipService.deleteById(id);
        tipBean.setTipIsDeleted(true);
        tipBean.setTips(getAllTips());
        tipBean.setImages(imageService.getAll());
    }

    //write dedicate query
    @Override
    public Iterable<TipData> getAllTips() {
        Iterable<TipModel> allTipsFromDatabase = tipService.getAll();
        ArrayList<TipData> convertedTipsWithoutRemoved = new ArrayList<>();
        allTipsFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedTipsWithoutRemoved.add(tipConverter.convert(e));
            }
        });
        return convertedTipsWithoutRemoved;
    }

    @Override
    public TipData getTipById(Long id) {
        return tipConverter.convert(tipService.getTipById(id));
    }

    @Override
    public TipModalData getTipModalById(Long id) {
        TipModel model = tipService.getTipById(id);
        TipModalData modalData = tipModalConverter.convert(model);
        return modalData;
    }
}
