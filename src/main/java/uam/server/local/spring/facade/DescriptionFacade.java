package uam.server.local.spring.facade;

import uam.server.local.data.impl.DescriptionData;

public interface DescriptionFacade {

    void addDescription(DescriptionData descriptionData) throws Exception;

    void editDescription(DescriptionData descriptionData) throws Exception;

    void deleteDescription(Long id);

    DescriptionData getDescriptionById(Long id);

    Iterable<DescriptionData> getAllDescriptions();
}
