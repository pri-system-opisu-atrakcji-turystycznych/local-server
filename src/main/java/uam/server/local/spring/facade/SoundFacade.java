package uam.server.local.spring.facade;

import uam.server.local.data.impl.SoundData;

public interface SoundFacade {

    void addSound(SoundData soundData);

    void editSound(SoundData soundData);

    void deleteSound(SoundData soundData);
}
