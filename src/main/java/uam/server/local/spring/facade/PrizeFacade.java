package uam.server.local.spring.facade;

import uam.server.local.data.impl.PrizeData;

public interface PrizeFacade {

    PrizeData getPrize();

    void addPrizes(final int quantity);


    void prepareDefaultView();


    void deletePrize(Long id);
}
