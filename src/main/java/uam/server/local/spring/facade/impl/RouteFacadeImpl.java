package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uam.server.local.data.impl.RouteData;
import uam.server.local.modalData.RouteModalData;
import uam.server.local.model.ImageModel;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.controller.beans.RouteBean;
import uam.server.local.spring.converter.impl.RouteConverter;
import uam.server.local.spring.converter.impl.RouteModalConverter;
import uam.server.local.spring.facade.RouteFacade;
import uam.server.local.spring.service.ImageService;
import uam.server.local.spring.service.RouteService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class RouteFacadeImpl implements RouteFacade {

    @Resource
    private RouteBean routeBean;

    @Resource
    private RouteService routeService;

    @Resource
    private RouteConverter routeConverter;

    @Resource
    private RouteModalConverter routeModalConverter;

    @Resource
    private ImageService imageService;

    public void addRoute(RouteModalData routeData) throws Exception {
        if (routeData.getPhoto() !=null && routeData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(routeData.getPhoto().getBytes());
            image = imageService.save(image);
            RouteModel model = routeModalConverter.inverseConvert(routeData);
            model.setPhoto(image.getId());
            routeService.save(model);
        } else {
            RouteModel model = routeModalConverter.inverseConvert(routeData);
            routeService.save(model);
        }

        routeBean.setRoutes(getAllRoutes());
    }

    public void editRoute(RouteModalData routeData) throws Exception {
        if (routeData.getPhoto() !=null && routeData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(routeData.getPhoto().getBytes());
            image = imageService.save(image);
            RouteModel model = routeModalConverter.inverseConvert(routeData);
            model.setPhoto(image.getId());
            routeService.save(model);
        } else {
            ImageModel image = imageService.getImageById(routeService.getRouteById(routeData.getId()).getPhoto());
            RouteModel model = routeModalConverter.inverseConvert(routeData);
            model.setPhoto(image.getId());
            routeService.save(model);
        }
        routeBean.setIsRouteEdit(true);
        routeBean.setRoutes(getAllRoutes());
    }

    public void deleteRoute(Long id) {
        routeService.deleteById(id);

        routeBean.setIsRouteDelete(true);
        routeBean.setRoutes(getAllRoutes());
    }

    @Override
    public Iterable<RouteData> getAllRoutes() {
        Iterable<RouteModel> allRoutesFromDatabase = routeService.getAll();
        ArrayList<RouteData> convertedRoutesWithoutRemoved = new ArrayList<>();
        allRoutesFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedRoutesWithoutRemoved.add(routeConverter.convert(e));
            }
        });
        return convertedRoutesWithoutRemoved;
    }

    @Override
    public RouteData getRouteById(Long id) {
        return routeConverter.convert(routeService.getRouteById(id));
    }

    @Override
    public Object getModalRouteById(Long id) {
        RouteModel model = routeService.getRouteById(id);
        RouteModalData modalData = routeModalConverter.convert(model);
        return modalData;
    }
}
