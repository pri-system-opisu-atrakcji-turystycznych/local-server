package uam.server.local.spring.facade;

import uam.server.local.data.impl.QuestionData;

public interface QuestionFacade {
    void addQuestion(QuestionData questionData);

    void editQuestion(QuestionData questionData);

    void deleteQuestion(Long id);

    Iterable<QuestionData> getAllQuestions();

    QuestionData getQuestionById(Long id);
}
