package uam.server.local.spring.facade;

import uam.server.local.data.impl.ElementData;
import uam.server.local.modalData.ElementModalData;

public interface ElementFacade {

    void addElement(ElementModalData elementData) throws Exception;

    void editElement(ElementModalData elementData) throws Exception;

    void deleteElement(Long id);

    ElementData getElementById(Long id);

    ElementModalData getElementModalById(Long id);
}
