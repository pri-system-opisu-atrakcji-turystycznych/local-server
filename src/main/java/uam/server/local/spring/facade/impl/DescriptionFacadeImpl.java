package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.DescriptionData;
import uam.server.local.model.DescriptionModel;
import uam.server.local.spring.controller.beans.DescriptionBean;
import uam.server.local.spring.converter.impl.DescriptionConverter;
import uam.server.local.spring.facade.DescriptionFacade;
import uam.server.local.spring.service.DescriptionService;
import uam.server.local.spring.service.ElementService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class DescriptionFacadeImpl implements DescriptionFacade {

    @Resource
    private DescriptionBean descriptionBean;

    @Resource
    private DescriptionService descriptionService;

    @Resource
    private ElementService elementService;

    @Resource
    private DescriptionConverter descriptionConverter;

    @Override
    public void addDescription(DescriptionData descriptionData) throws Exception {
        DescriptionModel model = descriptionConverter.inverseConvert(descriptionData);

        model.setElement(elementService.getElementFromDescription(model));
        descriptionService.save(model);
        descriptionBean.setDescriptions(getAllDescriptions());
        descriptionBean.setElements(elementService.getAll());

    }

    @Override
    public void editDescription(DescriptionData descriptionData) throws Exception {
        DescriptionModel model = descriptionConverter.inverseConvert(descriptionData);

        model.setElement(elementService.getElementFromDescription(model));

        descriptionService.save(model);
        descriptionBean.setDescriptions(getAllDescriptions());
        descriptionBean.setDescriptionIsEdit(true);
        descriptionBean.setElements(elementService.getAll());
    }

    @Override
    public void deleteDescription(Long id) {


        descriptionService.delete(id);
        descriptionBean.setDescriptions(getAllDescriptions());
        descriptionBean.setDescriptionIsDelete(true);
        descriptionBean.setElements(elementService.getAll());

    }

    @Override
    public DescriptionData getDescriptionById(Long id) {
        return descriptionConverter.convert(descriptionService.getDescriptionById(id));
    }

    @Override
    public Iterable<DescriptionData> getAllDescriptions() {
        Iterable<DescriptionModel> allDescriptionFromDatabase = descriptionService.getAll();
        ArrayList<DescriptionData> convertedDescriptionWithoutRemoved = new ArrayList<>();
        allDescriptionFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedDescriptionWithoutRemoved.add(descriptionConverter.convert(e));
            }
        });
        return convertedDescriptionWithoutRemoved;
    }
}
