package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.SoundData;
import uam.server.local.spring.converter.impl.SoundConverter;
import uam.server.local.spring.facade.SoundFacade;
import uam.server.local.spring.service.SoundService;

import javax.annotation.Resource;

@Service
public class SoundFacadeImpl implements SoundFacade {

    @Resource
    private SoundService soundService;

    @Resource
    private SoundConverter soundConverter;

    @Override
    public void addSound(SoundData soundData) {

    }

    @Override
    public void editSound(SoundData soundData) {

    }

    @Override
    public void deleteSound(SoundData soundData) {

    }
}
