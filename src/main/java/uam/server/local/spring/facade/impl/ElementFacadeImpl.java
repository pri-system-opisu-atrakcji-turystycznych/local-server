package uam.server.local.spring.facade.impl;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import uam.server.local.data.impl.ElementData;
import uam.server.local.modalData.ElementModalData;
import uam.server.local.model.ElementModel;
import uam.server.local.model.ImageModel;
import uam.server.local.spring.controller.beans.ElementBean;
import uam.server.local.spring.converter.impl.ElementConverter;
import uam.server.local.spring.converter.impl.ElementModalConverter;
import uam.server.local.spring.facade.ElementFacade;
import uam.server.local.spring.service.*;

import javax.annotation.Resource;

@Service
public class ElementFacadeImpl implements ElementFacade {
    @Resource
    private ElementBean elementBean;

    @Resource
    private ElementService elementService;

    @Resource
    private QuestionService questionService;

    @Resource
    private TipService tipService;

    @Resource
    private ElementConverter elementConverter;

    @Resource
    private ImageService imageService;

    @Resource
    private ElementModalConverter elementModalConverter;

    @Override
    public void addElement(ElementModalData elementData) throws Exception {
        if (elementData.getPhoto() !=null && elementData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(elementData.getPhoto().getBytes());
            image = imageService.save(image);
            ElementModel model = prepareElement(elementData);
            model.setPhoto(image.getId());
            elementService.save(model);
        } else {
            ElementModel model = prepareElement(elementData);
            elementService.save(model);
        }
        setBeanDataAfterOperation();
    }

    @Override
    public void editElement(ElementModalData elementData) throws Exception {
        if (elementData.getPhoto() !=null && elementData.getPhoto().getSize() > 0) {
            ImageModel image = new ImageModel();
            image.setPhoto(elementData.getPhoto().getBytes());
            image = imageService.save(image);
            ElementModel model = prepareElement(elementData);
            model.setPhoto(image.getId());
            elementService.save(model);
        } else {
            ImageModel image = imageService.getImageById(elementService.getElementById(elementData.getId()).getPhoto());
            ElementModel model = prepareElement(elementData);
            model.setPhoto(image.getId());
            elementService.save(model);
        }
        elementBean.setElementIsEdit(true);
        setBeanDataAfterOperation();
    }

    @NotNull
    private ElementModel prepareElement(ElementModalData elementData) {
        ElementModel model = elementModalConverter.inverseConvert(elementData);
        if (model.getQuestion() != null) {
            model.setQuestion(questionService.getQuestionById(elementData.getQuestion().getId()));
        }
        if (model.getTip().getId() != null) {
            model.setTip(tipService.getTipById(elementData.getTip().getId()));
        }
        return model;
    }

    @Override
    public void deleteElement(Long id) {
        elementService.deleteById(id);
        elementBean.setElementIsDelete(true);
        setBeanDataAfterOperation();

    }

    @Override
    public ElementData getElementById(Long id) {
        return elementConverter.convert(elementService.getElementById(id));
    }

    @Override
    public ElementModalData getElementModalById(Long id) {
        ElementModel model = elementService.getElementById(id);
        ElementModalData modalData = elementModalConverter.convert(model);
        return modalData;
    }

    private void setBeanDataAfterOperation() {
        elementBean.setQuestionsWithoutElement(questionService.getAllQuestionsWithoutElement());
        elementBean.setImages(imageService.getAll());
        elementBean.setTip(tipService.getAllNotRemoved());
        elementBean.setElements(elementService.getAllNotRemoved());
    }
}
