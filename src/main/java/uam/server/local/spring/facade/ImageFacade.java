package uam.server.local.spring.facade;

import uam.server.local.data.impl.ImageData;


public interface ImageFacade {

    void addImage(ImageData imageData);

    void editImage(ImageData imageData);

    void deleteImage(ImageData imageData);
}
