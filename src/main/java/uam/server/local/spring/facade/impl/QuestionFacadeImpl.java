package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.controller.beans.QuestionBean;
import uam.server.local.spring.converter.impl.QuestionConverter;
import uam.server.local.spring.facade.QuestionFacade;
import uam.server.local.spring.service.QuestionService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class QuestionFacadeImpl implements QuestionFacade {

    @Resource
    private QuestionBean questionBean;

    @Resource
    private QuestionService questionService;

    @Resource
    private QuestionConverter questionConverter;

    public void addQuestion(QuestionData questionData) {
        QuestionModel model = questionConverter.inverseConvert(questionData);

        questionService.save(model);
        questionBean.setQuestions(getAllQuestions());
    }

    @Override
    public void editQuestion(QuestionData questionData) {
        QuestionModel model = questionConverter.inverseConvert(questionData);
        questionService.save(model);
        questionBean.setIsQuesitonEdit(true);
        questionBean.setQuestions(getAllQuestions());

    }

    public void deleteQuestion(Long  id) {
        questionService.deleteById(id);
        questionBean.setIsQuestionDelete(true);
        questionBean.setQuestions(getAllQuestions());
    }

    @Override
    public Iterable<QuestionData> getAllQuestions() {
        Iterable<QuestionModel> allMessageFromDatabase = questionService.getAll();
        ArrayList<QuestionData> convertedMessagesWithoutRemoved = new ArrayList<>();
        allMessageFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedMessagesWithoutRemoved.add(questionConverter.convert(e));
            }
        });
        return convertedMessagesWithoutRemoved;
    }

    @Override
    public QuestionData getQuestionById(Long id) {
        return questionConverter.convert(questionService.getQuestionById(id));
    }
}
