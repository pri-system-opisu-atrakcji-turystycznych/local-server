package uam.server.local.spring.facade;

import uam.server.local.data.impl.TipData;
import uam.server.local.modalData.TipModalData;

import java.io.IOException;

public interface TipFacade {
    void addTip(TipModalData tipData) throws Exception;

    void editTip(TipModalData tipData) throws Exception;

    void deleteTip(Long id);

    Iterable<TipData> getAllTips();

    TipData getTipById(Long id);

    TipModalData getTipModalById(Long id);
}
