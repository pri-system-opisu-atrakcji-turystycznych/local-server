package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.ImageData;
import uam.server.local.spring.converter.impl.ImageConverter;
import uam.server.local.spring.facade.ImageFacade;
import uam.server.local.spring.service.ImageService;

import javax.annotation.Resource;

@Service
public class ImageFacadeImpl implements ImageFacade {
    @Resource
    private ImageService imageService;

    @Resource
    private ImageConverter imageConverter;

    @Override
    public void addImage(ImageData imageData) {

    }

    @Override
    public void editImage(ImageData imageData) {

    }

    @Override
    public void deleteImage(ImageData imageData) {

    }
}
