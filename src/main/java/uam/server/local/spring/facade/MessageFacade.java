package uam.server.local.spring.facade;

import uam.server.local.data.impl.MessageData;

public interface MessageFacade {

    void addMessage(MessageData messageData);

    void editMessage(MessageData messageData);

    void deleteMessage(Long id);

    Iterable<MessageData> getAllMessages();

    MessageData getMessageById(Long id);
}
