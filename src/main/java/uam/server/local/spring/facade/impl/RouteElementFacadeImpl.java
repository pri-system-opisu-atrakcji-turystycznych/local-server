package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.RouteElementData;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.controller.beans.RouteElementBean;
import uam.server.local.spring.converter.impl.RouteElementConverter;
import uam.server.local.spring.facade.RouteElementFacade;
import uam.server.local.spring.service.ElementService;
import uam.server.local.spring.service.RouteElementService;
import uam.server.local.spring.service.RouteService;

import javax.annotation.Resource;

@Service
public class RouteElementFacadeImpl implements RouteElementFacade {

    @Resource
    private RouteElementBean routeElementBean;

    @Resource
    private RouteElementService routeElementService;

    @Resource
    private ElementService elementService;

    @Resource
    private RouteElementConverter routeElementConverter;
    @Resource
    private RouteService routeService;

    public void addRouteElement(RouteElementData routeElementData) {
        RouteElementModel model = routeElementConverter.inverseConvert(routeElementData);

        routeElementService.save(model);

        routeElementBean.setRoutesElements(routeElementService.getAllNotRemoved());
        routeElementBean.setAllElements(elementService.getAllNotRemoved());
        routeElementBean.setRoutes(routeService.getAllNotRemoved());
    }

    @Override
    public void editRouteElement(RouteElementData routeElementData) {
        RouteElementModel model = routeElementConverter.inverseConvert(routeElementData);

        routeElementService.save(model);

        routeElementBean.setIsRouteElementEdit(true);
        routeElementBean.setRoutesElements(routeElementService.getAllNotRemoved());
        routeElementBean.setAllElements(elementService.getAllNotRemoved());
        routeElementBean.setRoutes(routeService.getAllNotRemoved());
    }

    public void deleteRouteElement(long id) {
        routeElementService.deleteById(id);

        routeElementBean.setIsRouteElementDelete(true);
        routeElementBean.setRoutesElements(routeElementService.getAllNotRemoved());
        routeElementBean.setAllElements(elementService.getAllNotRemoved());
        routeElementBean.setRoutes(routeService.getAllNotRemoved());
    }

    @Override
    public RouteElementData getMessageById(Long id) {
        return routeElementConverter.convert(routeElementService.getRouteElementById(id));
    }
}
