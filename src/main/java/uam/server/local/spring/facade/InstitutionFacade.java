package uam.server.local.spring.facade;

import uam.server.local.data.impl.InstitutionData;

public interface InstitutionFacade {
    void addInstitution(InstitutionData institutionData);

    void editInstitution(InstitutionData institutionData);

    void deleteInstitution(Long id);

    Iterable<InstitutionData> getAllInstitution();

    InstitutionData getInstitutionByNameIsNotNull();

    InstitutionData getMessageById(Long id);
}
