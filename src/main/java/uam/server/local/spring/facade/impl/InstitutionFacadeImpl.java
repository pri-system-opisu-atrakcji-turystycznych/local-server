package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.InstitutionData;
import uam.server.local.model.InstitutionModel;
import uam.server.local.spring.controller.beans.InstitutionBean;
import uam.server.local.spring.converter.impl.InstitutionConverter;
import uam.server.local.spring.facade.InstitutionFacade;
import uam.server.local.spring.service.InstitutionService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class InstitutionFacadeImpl implements InstitutionFacade {

    @Resource
    private InstitutionBean institutionBean;

    @Resource
    private InstitutionService institutionService;

    @Resource
    private InstitutionConverter institutionConverter;

    public void addInstitution(InstitutionData institutionData) {
        InstitutionModel model = institutionConverter.inverseConvert(institutionData);
        institutionService.save(model);
        institutionBean.setInstitutions(getAllInstitution());
    }

    @Override
    public void editInstitution(InstitutionData institutionData) {
        InstitutionModel model = institutionConverter.inverseConvert(institutionData);
        institutionService.save(model);

        institutionBean.setIsInstitutionEdit(true);
        institutionBean.setInstitutions(getAllInstitution());


    }


    public void deleteInstitution(Long id) {
        institutionService.deleteById(id);
        institutionBean.setIsInstitutionDelete(true);
        institutionBean.setInstitutions(getAllInstitution());

    }

    @Override
    public Iterable<InstitutionData> getAllInstitution() {
        Iterable<InstitutionModel> allInstitutionFromDatabase = institutionService.getAll();
        ArrayList<InstitutionData> convertedInstitutionWithoutRemoved = new ArrayList<>();
        allInstitutionFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedInstitutionWithoutRemoved.add(institutionConverter.convert(e));
            }
        });
        return convertedInstitutionWithoutRemoved;
    }

    @Override
    public InstitutionData getInstitutionByNameIsNotNull() {
        return institutionConverter.convert(institutionService.getInstitutionByNameIsNotNull());
    }

    @Override
    public InstitutionData getMessageById(Long id) {
        return institutionConverter.convert(institutionService.getInstitutionById(id));
    }
}
