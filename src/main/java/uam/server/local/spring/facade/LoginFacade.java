package uam.server.local.spring.facade;

import uam.server.local.model.UserModel;

public interface LoginFacade {

    void register(final UserModel user);

    void logIn();

    void editOwnAccount(final UserModel newUserData);

    void editUserByAdmin(final String email, final UserModel dataToChange);

    void deleteUser(final String email);
}
