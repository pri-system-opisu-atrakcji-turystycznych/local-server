package uam.server.local.spring.facade;

import uam.server.local.data.impl.RouteData;
import uam.server.local.modalData.RouteModalData;

public interface RouteFacade {
    void addRoute(RouteModalData routeData) throws Exception;
    void editRoute(RouteModalData routeData) throws Exception;
    void deleteRoute(Long id);

    Iterable<RouteData> getAllRoutes();

    RouteData getRouteById(Long id);

    Object getModalRouteById(Long id);
}
