package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.MessageData;
import uam.server.local.model.MessageModel;
import uam.server.local.spring.controller.beans.MessageBean;
import uam.server.local.spring.converter.impl.MessageConverter;
import uam.server.local.spring.facade.MessageFacade;
import uam.server.local.spring.service.MessageService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class MessageFacadeImpl implements MessageFacade {
    @Resource
    private MessageBean messageBean;

    @Resource
    private MessageService messageService;

    @Resource
    private MessageConverter messageConverter;

    public void addMessage(MessageData messageData) {
        MessageModel model = messageConverter.inverseConvert(messageData);

        messageService.save(model);

        messageBean.setMessages(getAllMessages());
    }

    @Override
    public void editMessage(MessageData messageData) {
        MessageModel model = messageConverter.inverseConvert(messageData);

        messageService.save(model);

        messageBean.setIsMessageEdit(true);
        messageBean.setMessages(getAllMessages());

    }

    @Override
    public void deleteMessage(Long id) {

        messageService.deleteById(id);
        messageBean.setIsMessageDelete(true);
        messageBean.setMessages(getAllMessages());
    }

    @Override
    public Iterable<MessageData> getAllMessages() {
        Iterable<MessageModel> allMessageFromDatabase = messageService.getAll();
        ArrayList<MessageData> convertedMessagesWithoutRemoved = new ArrayList<>();
        allMessageFromDatabase.forEach(e -> {
            if (!e.getRemoved()) {
                convertedMessagesWithoutRemoved.add(messageConverter.convert(e));
            }
        });
        return convertedMessagesWithoutRemoved;
    }

    @Override
    public MessageData getMessageById(Long id) {
        return messageConverter.convert(messageService.getMessageById(id));
    }
}
