package uam.server.local.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.PrizeData;
import uam.server.local.model.PrizeModel;
import uam.server.local.spring.controller.beans.PrizeBean;
import uam.server.local.spring.converter.Converter;
import uam.server.local.spring.facade.PrizeFacade;
import uam.server.local.spring.service.PrizeService;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class PrizeFacadeImpl implements PrizeFacade {
    @Resource
    private PrizeBean prizeBean;
    @Resource
    private PrizeService prizeService;
    @Resource
    private Converter<PrizeData, PrizeModel> prizeDataPrizeModelConverter;

    @Override
    public PrizeData getPrize() {
        return prizeDataPrizeModelConverter.convert(prizeService.getPrize());
    }

    @Override
    public void addPrizes(final int quantity) {
        prizeService.addPrizes(quantity);

        prizeBean.setPrizes(getPrizesData(prizeService.getPrizes()));
    }


    @Override
    public void prepareDefaultView() {
        prizeBean.setPrizes(getPrizesData(prizeService.getPrizes()));
    }

    @Override
    public void deletePrize(Long id) {
        prizeService.deleteById(id);
        prizeBean.setPrizes(getPrizesData(prizeService.getPrizes()));

    }

    private Iterable<PrizeData> getPrizesData(Iterable<PrizeModel> prizesModel){
        ArrayList<PrizeData> prizesData = new ArrayList<>();
        prizesModel.forEach(e ->
            prizesData.add(prizeDataPrizeModelConverter.convert(e)));
        return prizesData;
    }

}
