package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.DescriptionModel;

import java.util.Optional;

@Repository
public interface DescriptionRepository extends CrudRepository<DescriptionModel, Long>, PagingAndSortingRepository<DescriptionModel, Long> {

    Optional<DescriptionModel> findFirstByElement_IdAndMobileIsTrue(final Long id);

    Optional<DescriptionModel> findFirstByElement_IdAndWebIsTrue(final Long id);

    Optional<DescriptionModel> findFirstByElement_IdAndWebIsTrueAndMobileIsTrue(final Long id);

    Optional<DescriptionModel> findFirstByElement_IdAndWebIsFalseAndMobileIsFalse(final Long id);

}
