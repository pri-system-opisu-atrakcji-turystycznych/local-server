package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.SoundModel;

import java.util.Optional;

@Repository
public interface SoundRepository extends CrudRepository<SoundModel, Long>, PagingAndSortingRepository<SoundModel, Long> {

    Optional<SoundModel> findTopById(final Long id);

}