package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.repository.impl.customRepository.RouteElementRepositoryCustom;

@Repository
public interface RouteElementRepository extends CrudRepository<RouteElementModel, Long>, PagingAndSortingRepository<RouteElementModel, Long>, RouteElementRepositoryCustom {

    Iterable<RouteElementModel> findByRoute_Id(final Long id);

}
