package uam.server.local.spring.repository.impl.customRepository;

import uam.server.local.model.RouteModel;

public interface RouteRepositoryCustom {
    Iterable<RouteModel> getAllNotRemoved();
}
