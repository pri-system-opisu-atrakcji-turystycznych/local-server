package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.repository.impl.customRepository.QuestionRepositoryCustom;

import java.util.Optional;

@Repository
public interface QuestionRepository extends CrudRepository<QuestionModel, Long>, PagingAndSortingRepository<QuestionModel, Long>, QuestionRepositoryCustom {
    //   @Query(value = "Select q from QuestionModel q where q.id not in (select distinct  e.question from ElementModel e)")
    //   Iterable<QuestionModel> getAllQuestionsWithoutElement();

    Optional<QuestionModel> findTopById(final Long id);
}
