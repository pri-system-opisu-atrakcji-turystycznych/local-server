package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import uam.server.local.model.TipModel;
import uam.server.local.spring.repository.impl.customRepository.TipRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class TipRepositoryImpl implements TipRepositoryCustom {
    @Autowired
    EntityManager em;

    @Override
    public Iterable<TipModel> getAllNotRemoved() {
        TypedQuery<TipModel> query = em.createQuery("Select t from TipModel t where t.removed <> true or t.removed is null", TipModel.class);
        List<TipModel> q = query.getResultList();
        return q;
    }
}
