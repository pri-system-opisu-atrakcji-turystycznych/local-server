package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.PrizeModel;

import java.util.Optional;

@Repository
public interface PrizeRepository extends CrudRepository<PrizeModel, Long>, PagingAndSortingRepository<PrizeModel, Long> {

    Optional<PrizeModel> findTopByActiveIsTrue();

    Iterable<PrizeModel> findAllByActiveIsTrueOrderByIdDesc();
}
