package uam.server.local.spring.repository.impl.customRepository;

public interface UserRepositoryCustom {

    Boolean existsByEmail(final String email);
}
