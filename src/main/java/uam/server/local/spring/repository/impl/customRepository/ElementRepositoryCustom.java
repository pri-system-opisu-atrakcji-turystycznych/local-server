package uam.server.local.spring.repository.impl.customRepository;

import uam.server.local.model.ElementModel;

public interface ElementRepositoryCustom {
    Iterable<ElementModel> getAllNotRemoved();
}
