package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.ElementModel;
import uam.server.local.model.TipModel;
import uam.server.local.spring.repository.impl.customRepository.ElementRepositoryCustom;

@Repository
public interface ElementRepository extends CrudRepository<ElementModel, Long>, PagingAndSortingRepository<ElementModel, Long>, ElementRepositoryCustom {
    Iterable<ElementModel> findByTip(TipModel tip);
}
