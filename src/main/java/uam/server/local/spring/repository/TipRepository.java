package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.TipModel;
import uam.server.local.spring.repository.impl.customRepository.TipRepositoryCustom;

import java.util.Optional;

@Repository
public interface TipRepository extends CrudRepository<TipModel, Long>, PagingAndSortingRepository<TipModel, Long>, TipRepositoryCustom {

    Optional<TipModel> findById(final Long id);

}
