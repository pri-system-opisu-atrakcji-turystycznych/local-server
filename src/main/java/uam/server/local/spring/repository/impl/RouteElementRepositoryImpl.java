package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.repository.impl.customRepository.RouteElementRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RouteElementRepositoryImpl implements RouteElementRepositoryCustom {
    @Autowired
    EntityManager em;

    @Override
    public Iterable<RouteElementModel> getAllNotRemoved() {
        TypedQuery<RouteElementModel> query = em.createQuery("Select r from RouteElementModel r where r.removed <> true or r.removed is null", RouteElementModel.class);
        List<RouteElementModel> q = query.getResultList();
        return q;
    }
}
