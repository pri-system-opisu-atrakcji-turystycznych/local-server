package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.UserModel;
import uam.server.local.spring.repository.impl.customRepository.UserRepositoryCustom;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long>, PagingAndSortingRepository<UserModel, Long>, UserRepositoryCustom {

    Optional<UserModel> findByEmail(final String email);

}
