package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.repository.impl.customRepository.QuestionRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class QuestionRepositoryImpl implements QuestionRepositoryCustom {
    @Autowired
    EntityManager em;

    @Override
    public Iterable<QuestionModel> getAllWithoutElement() {
        TypedQuery<QuestionModel> query = em.createQuery("Select q from QuestionModel q where (q.removed <> true or q.removed is null )and q.id not in (select distinct e.question from ElementModel e)", QuestionModel.class);
        List<QuestionModel> q = query.getResultList();
        return q;
    }
}
