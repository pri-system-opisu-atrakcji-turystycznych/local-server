package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.MessageModel;

@Repository
public interface MessageRepository extends CrudRepository<MessageModel, Long>, PagingAndSortingRepository<MessageModel, Long> {
}
