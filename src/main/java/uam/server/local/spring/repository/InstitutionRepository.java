package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.InstitutionModel;

import java.util.Optional;

@Repository
public interface InstitutionRepository extends CrudRepository<InstitutionModel, Long>, PagingAndSortingRepository<InstitutionModel, Long> {

    Optional<InstitutionModel> findFirstByNameIsNotNull();

}
