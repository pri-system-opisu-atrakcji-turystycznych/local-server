package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.repository.impl.customRepository.RouteRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RouteRepositoryImpl implements RouteRepositoryCustom {
    @Autowired
    EntityManager em;
    @Override
    public Iterable<RouteModel> getAllNotRemoved() {
        TypedQuery<RouteModel> query = em.createQuery("Select r from RouteModel r where r.removed <> true or r.removed is null", RouteModel.class);
        List<RouteModel> q = query.getResultList();
        return q;
    }
}
