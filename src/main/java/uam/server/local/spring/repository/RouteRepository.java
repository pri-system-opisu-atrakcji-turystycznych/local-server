package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.repository.impl.customRepository.RouteRepositoryCustom;

import java.util.Optional;

@Repository
public interface RouteRepository extends CrudRepository<RouteModel, Long>, PagingAndSortingRepository<RouteModel, Long>, RouteRepositoryCustom {

    Optional<RouteModel> findTopById(final Long id);

}
