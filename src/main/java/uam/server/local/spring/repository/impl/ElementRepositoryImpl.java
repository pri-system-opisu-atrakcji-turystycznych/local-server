package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import uam.server.local.model.ElementModel;
import uam.server.local.spring.repository.impl.customRepository.ElementRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ElementRepositoryImpl implements ElementRepositoryCustom {
    @Autowired
    EntityManager em;

    @Override
    public Iterable<ElementModel> getAllNotRemoved() {
        TypedQuery<ElementModel> query = em.createQuery("Select e from ElementModel e where e.removed <> true or e.removed is null", ElementModel.class);
        List<ElementModel> q = query.getResultList();
        return q;
    }
}
