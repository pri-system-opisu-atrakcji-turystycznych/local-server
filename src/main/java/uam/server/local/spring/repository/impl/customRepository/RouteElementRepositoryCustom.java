package uam.server.local.spring.repository.impl.customRepository;

import uam.server.local.model.RouteElementModel;

public interface RouteElementRepositoryCustom {
    Iterable<RouteElementModel> getAllNotRemoved();
}
