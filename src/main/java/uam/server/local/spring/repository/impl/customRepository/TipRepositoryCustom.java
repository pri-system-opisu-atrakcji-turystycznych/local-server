package uam.server.local.spring.repository.impl.customRepository;

import uam.server.local.model.TipModel;

public interface TipRepositoryCustom {

    Iterable<TipModel> getAllNotRemoved();
}
