package uam.server.local.spring.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import uam.server.local.model.UserModel;
import uam.server.local.spring.repository.impl.customRepository.UserRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {
    @Autowired
    EntityManager em;

    @Override
    public Boolean existsByEmail(final String email) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<String> criteria = builder.createQuery(String.class);
        Root<UserModel> personRoot = criteria.from(UserModel.class);
        criteria.select(personRoot.get("email"));
        criteria.where(builder.equal(personRoot.get("email"), email));
        List<String> users = em.createQuery(criteria).getResultList();
        return !users.isEmpty();
    }
}
