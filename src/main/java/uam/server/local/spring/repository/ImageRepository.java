package uam.server.local.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.local.model.ImageModel;

import java.util.Optional;

@Repository
public interface ImageRepository extends CrudRepository<ImageModel, Long>, PagingAndSortingRepository<ImageModel, Long> {

    Optional<ImageModel> findTopById(final Long id);

}
