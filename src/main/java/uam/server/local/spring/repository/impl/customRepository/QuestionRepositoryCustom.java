package uam.server.local.spring.repository.impl.customRepository;

import org.springframework.stereotype.Repository;
import uam.server.local.model.QuestionModel;

@Repository
public interface QuestionRepositoryCustom {

    Iterable<QuestionModel> getAllWithoutElement();

}
