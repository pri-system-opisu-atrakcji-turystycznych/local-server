package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.TipData;
import uam.server.local.model.TipModel;
import uam.server.local.spring.converter.Converter;

@Service
public class TipConverter implements Converter<TipData, TipModel> {

    @Override
    public TipData convert(TipModel model) {
        return TipData.builder()
                .id(model.getId())
                .description(model.getDescription())
                .photo(model.getPhoto())
                .build();
    }

    @Override
    public TipModel inverseConvert(TipData data) {
        if(data !=null){

            return TipModel.builder()
                    .id(data.getId())
                    .description(data.getDescription())
                    .photo(data.getPhoto())
                    .build();
        }
    return null;
    }
}
