package uam.server.local.spring.converter;

import uam.server.local.data.Data;
import uam.server.local.model.Model;

public interface Converter<D extends Data, M extends Model> {

    D convert(M model);

    M inverseConvert(D data);
}
