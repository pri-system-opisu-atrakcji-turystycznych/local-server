package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.ImageData;
import uam.server.local.model.ImageModel;
import uam.server.local.spring.converter.Converter;

@Service
public class ImageConverter implements Converter<ImageData, ImageModel> {

    @Override
    public ImageData convert(ImageModel model) {
        return ImageData.builder()
                .id(model.getId())
                .photo(model.getPhoto())
                .build();
    }

    @Override
    public ImageModel inverseConvert(ImageData data) {
        return ImageModel.builder()
                .id(data.getId())
                .photo(data.getPhoto())
                .build();
    }

}
