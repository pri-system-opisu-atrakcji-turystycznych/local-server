package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.RoleData;
import uam.server.local.model.RoleModel;
import uam.server.local.spring.converter.Converter;

@Service
public class RoleConverter implements Converter<RoleData, RoleModel> {

    @Override
    public RoleData convert(RoleModel model) {
        return RoleData.builder()
                .id(model.getId())
                .role(model.getRole())
                .build();
    }

    @Override
    public RoleModel inverseConvert(RoleData data) {
        return RoleModel.builder()
                .id(data.getId())
                .role(data.getRole())
                .build();
    }

}
