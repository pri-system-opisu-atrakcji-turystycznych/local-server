package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.RouteElementData;
import uam.server.local.model.RouteElementModel;
import uam.server.local.spring.converter.Converter;
@Service
public class RouteElementConverter implements Converter<RouteElementData, RouteElementModel> {

    private ElementConverter elementConverter;
    private RouteConverter routeConverter;

    public RouteElementConverter(ElementConverter elementConverter, RouteConverter routeConverter) {
        this.elementConverter = elementConverter;
        this.routeConverter = routeConverter;
    }

    @Override
    public RouteElementData convert(RouteElementModel model) {
        return RouteElementData.builder()
                .id(model.getId())
                .element(elementConverter.convert(model.getElement()))
                .route(new RouteConverter().convert(model.getRoute()))
                .number(model.getNumber())
                .build();
    }

    @Override
    public RouteElementModel inverseConvert(RouteElementData data) {
        if (data != null) {

            return RouteElementModel.builder()
                    .id(data.getId())
                    .element(elementConverter.inverseConvert(data.getElement()))
                    .route(routeConverter.inverseConvert(data.getRoute()))
                    .number(data.getNumber())
                    .build();
        }
        return null;
    }
}
