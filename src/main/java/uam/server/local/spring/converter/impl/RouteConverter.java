package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.RouteData;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.converter.Converter;

@Service
public class RouteConverter implements Converter<RouteData, RouteModel> {

    @Override
    public RouteData convert(RouteModel model) {
        return RouteData.builder()
                .id(model.getId())
                .name(model.getName())
                .description(model.getDescription())
                .photo(model.getPhoto())
                .build();
    }

    @Override
    public RouteModel inverseConvert(RouteData data) {
        return RouteModel.builder()
                .id(data.getId())
                .name(data.getName())
                .description(data.getDescription())
                .photo(data.getPhoto())
                .build();
    }

}
