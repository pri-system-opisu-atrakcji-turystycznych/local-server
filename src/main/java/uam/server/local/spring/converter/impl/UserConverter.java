package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.RoleData;
import uam.server.local.data.impl.UserData;
import uam.server.local.model.RoleModel;
import uam.server.local.model.UserModel;
import uam.server.local.spring.converter.Converter;

import javax.annotation.Resource;
import java.util.stream.Collectors;

@Service
public class UserConverter implements Converter<UserData, UserModel> {

    @Resource
    private Converter<RoleData, RoleModel> roleDataRoleModelConverter;

    @Override
    public UserData convert(UserModel model) {
        return UserData.builder()
                .id(model.getId())
                .email(model.getEmail())
                .password(model.getPassword())
                .active(model.getActive())
                .roles(model.getRoles().stream()
                        .map(r -> roleDataRoleModelConverter.convert(r))
                        .collect(Collectors.toSet()))
                .build();
    }

    @Override
    public UserModel inverseConvert(UserData data) {
        return UserModel.builder()
                .id(data.getId())
                .email(data.getEmail())
                .password(data.getPassword())
                .active(data.getActive())
                .roles(data.getRoles().stream()
                        .map(r -> roleDataRoleModelConverter.inverseConvert(r))
                        .collect(Collectors.toSet()))
                .build();
    }

}
