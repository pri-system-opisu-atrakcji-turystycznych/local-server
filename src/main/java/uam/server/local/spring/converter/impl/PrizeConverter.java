package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.PrizeData;
import uam.server.local.model.PrizeModel;
import uam.server.local.spring.converter.Converter;

@Service
public class PrizeConverter implements Converter<PrizeData, PrizeModel> {

    @Override
    public PrizeData convert(PrizeModel model) {
        return PrizeData.builder()
                .id(model.getId())
                .code(model.getCode())
                .build();
    }

    @Override
    public PrizeModel inverseConvert(PrizeData data) {
        return PrizeModel.builder()
                .id(data.getId())
                .code(data.getCode())
                .build();
    }

}
