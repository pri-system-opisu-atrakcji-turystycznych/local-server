package uam.server.local.spring.converter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.data.impl.TipData;
import uam.server.local.modalData.ElementModalData;
import uam.server.local.model.ElementModel;
import uam.server.local.model.QuestionModel;
import uam.server.local.model.TipModel;
import uam.server.local.spring.converter.Converter;
@Service
public class ElementModalConverter implements Converter<ElementModalData, ElementModel> {
    private Converter<QuestionData, QuestionModel> questionDataQuestionModelConverter;
    private Converter<TipData, TipModel> tipDataTipModelConverter;

    @Autowired
    public ElementModalConverter(Converter<QuestionData, QuestionModel> questionDataQuestionModelConverter,
                            Converter<TipData, TipModel> tipDataTipModelConverter) {
        this.questionDataQuestionModelConverter = questionDataQuestionModelConverter;
        this.tipDataTipModelConverter = tipDataTipModelConverter;
    }
    @Override
    public ElementModalData convert(ElementModel model) {
        return ElementModalData.builder()
                .id(model.getId())
                .name(model.getName())
                .shortDescription(model.getShortDescription())
                .gpsX(model.getGpsX())
                .gpsY(model.getGpsY())
                .photo(null)
                .question(model.getQuestion() ==null ? null : questionDataQuestionModelConverter.convert(model.getQuestion()))
                .tip(model.getTip() ==null ? null : tipDataTipModelConverter.convert(model.getTip()))
                .build();
    }

    @Override
    public ElementModel inverseConvert(ElementModalData data) {
        return ElementModel.builder()
                .id(data.getId())
                .name(data.getName())
                .shortDescription(data.getShortDescription())
                .gpsX(data.getGpsX())
                .gpsY(data.getGpsY())
                .photo(null)
                .sound(null)
                .question(questionDataQuestionModelConverter.inverseConvert(data.getQuestion()))
                .tip(tipDataTipModelConverter.inverseConvert(data.getTip()))
                .build();
    }
}
