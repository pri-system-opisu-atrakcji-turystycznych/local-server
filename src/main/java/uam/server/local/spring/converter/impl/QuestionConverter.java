package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.model.QuestionModel;
import uam.server.local.spring.converter.Converter;

@Service
public class QuestionConverter implements Converter<QuestionData, QuestionModel> {

    @Override
    public QuestionData convert(QuestionModel model) {
        return QuestionData.builder()
                .id(model.getId())
                .content(model.getContent())
                .answerOne(model.getAnswerOne())
                .answerTwo(model.getAnswerTwo())
                .answerTree(model.getAnswerTree())
                .rightAnswer(model.getRightAnswer())
                .build();
    }

    @Override
    public QuestionModel inverseConvert(QuestionData data) {
        if (data != null){
            return QuestionModel.builder()
                    .id(data.getId())
                    .content(data.getContent())
                    .answerOne(data.getAnswerOne())
                    .answerTwo(data.getAnswerTwo())
                    .answerTree(data.getAnswerTree())
                    .rightAnswer(data.getRightAnswer())
                    .build();
        }
        return null;
    }

}
