package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.MessageData;
import uam.server.local.model.MessageModel;
import uam.server.local.spring.converter.Converter;

@Service
public class MessageConverter implements Converter<MessageData, MessageModel> {

    @Override
    public MessageData convert(MessageModel model) {
        return MessageData.builder()
                .id(model.getId())
                .title(model.getTitle())
                .description(model.getDescription())
                .build();
    }

    @Override
    public MessageModel inverseConvert(MessageData data) {
        return MessageModel.builder()
                .id(data.getId())
                .title(data.getTitle())
                .description(data.getDescription())
                .build();
    }

}
