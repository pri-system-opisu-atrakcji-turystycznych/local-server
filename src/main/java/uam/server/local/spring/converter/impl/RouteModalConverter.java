package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.modalData.RouteModalData;
import uam.server.local.model.QuestionModel;
import uam.server.local.model.RouteModel;
import uam.server.local.spring.converter.Converter;
@Service
public class RouteModalConverter implements Converter<RouteModalData, RouteModel> {
    @Override
    public RouteModalData convert(RouteModel model) {
        return RouteModalData.builder()
                .id(model.getId())
                .name(model.getName())
                .description(model.getDescription())
                .photo(null)
                .build();
    }

    @Override
    public RouteModel inverseConvert(RouteModalData data) {
        return RouteModel.builder()
                .id(data.getId())
                .name(data.getName())
                .description(data.getDescription())
                .photo(null)
                .build();
    }
}
