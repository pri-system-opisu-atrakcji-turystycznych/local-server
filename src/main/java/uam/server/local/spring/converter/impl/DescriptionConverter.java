package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.DescriptionData;
import uam.server.local.data.impl.ElementData;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;
import uam.server.local.spring.converter.Converter;

import javax.annotation.Resource;

@Service
public class DescriptionConverter implements Converter<DescriptionData, DescriptionModel> {

    @Resource
    private Converter<ElementData, ElementModel> elementDataElementModelConverter;

    @Override
    public DescriptionData convert(final DescriptionModel model) {
        return DescriptionData.builder()
                .id(model.getId())
                .element(elementDataElementModelConverter.convert(model.getElement()))
                .title(model.getTitle())
                .content(model.getContent())
                .mobile(model.getMobile())
                .web(model.getWeb())
                .build();
    }

    @Override
    public DescriptionModel inverseConvert(final DescriptionData data) {
        return DescriptionModel.builder()
                .id(data.getId())
                .element(elementDataElementModelConverter.inverseConvert(data.getElement()))
                .title(data.getTitle())
                .content(data.getContent())
                .mobile(data.getMobile())
                .web(data.getWeb())
                .build();
    }

}
