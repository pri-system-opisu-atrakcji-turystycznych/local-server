package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.SoundData;
import uam.server.local.model.SoundModel;
import uam.server.local.spring.converter.Converter;

@Service
public class SoundConverter implements Converter<SoundData, SoundModel> {

    @Override
    public SoundData convert(SoundModel model) {
        return SoundData.builder()
                .id(model.getId())
                .sound(model.getSound())
                .build();
    }

    @Override
    public SoundModel inverseConvert(SoundData data) {
        return SoundModel.builder()
                .id(data.getId())
                .sound(data.getSound())
                .build();
    }

}
