package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.data.impl.InstitutionData;
import uam.server.local.model.InstitutionModel;
import uam.server.local.spring.converter.Converter;

@Service
public class InstitutionConverter implements Converter<InstitutionData, InstitutionModel> {

    @Override
    public InstitutionData convert(InstitutionModel model) {
        return InstitutionData.builder()
                .id(model.getId())
                .name(model.getName())
                .description(model.getDescription())
                .location(model.getLocation())
                .phone(model.getPhone())
                .email(model.getEmail())
                .build();
    }

    @Override
    public InstitutionModel inverseConvert(InstitutionData data) {
        return InstitutionModel.builder()
                .id(data.getId())
                .name(data.getName())
                .description(data.getDescription())
                .location(data.getLocation())
                .phone(data.getPhone())
                .email(data.getEmail())
                .build();
    }

}
