package uam.server.local.spring.converter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uam.server.local.data.impl.ElementData;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.data.impl.TipData;
import uam.server.local.model.ElementModel;
import uam.server.local.model.QuestionModel;
import uam.server.local.model.TipModel;
import uam.server.local.spring.converter.Converter;

@Service
public class ElementConverter implements Converter<ElementData, ElementModel> {
    private Converter<QuestionData, QuestionModel> questionDataQuestionModelConverter;
    private Converter<TipData, TipModel> tipDataTipModelConverter;

    @Autowired
    public ElementConverter(Converter<QuestionData, QuestionModel> questionDataQuestionModelConverter,
                            Converter<TipData, TipModel> tipDataTipModelConverter) {
        this.questionDataQuestionModelConverter = questionDataQuestionModelConverter;
        this.tipDataTipModelConverter = tipDataTipModelConverter;
    }

    @Override
    public ElementData convert(ElementModel model) {
        return ElementData.builder()
                .id(model.getId())
                .name(model.getName())
                .shortDescription(model.getShortDescription())
                .gpsX(model.getGpsX())
                .gpsY(model.getGpsY())
                .photo(model.getPhoto())
                .sound(model.getSound())
                .question(questionDataQuestionModelConverter.convert(model.getQuestion()))
                .tip(tipDataTipModelConverter.convert(model.getTip()))
                .build();
    }

    @Override
    public ElementModel inverseConvert(ElementData data) {
        return ElementModel.builder()
                .id(data.getId())
                .name(data.getName())
                .shortDescription(data.getShortDescription())
                .gpsX(data.getGpsX())
                .gpsY(data.getGpsY())
                .photo(data.getPhoto())
                .sound(data.getSound())
                .question(questionDataQuestionModelConverter.inverseConvert(data.getQuestion()))
                .tip(tipDataTipModelConverter.inverseConvert(data.getTip()))
                .build();
    }

}
