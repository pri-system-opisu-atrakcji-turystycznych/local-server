package uam.server.local.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.local.modalData.TipModalData;
import uam.server.local.model.TipModel;
import uam.server.local.spring.converter.Converter;
@Service
public class TipModalConverter implements Converter<TipModalData, TipModel> {
    @Override
    public TipModalData convert(TipModel model) {
        return TipModalData.builder()
                .id(model.getId())
                .description(model.getDescription())
                .photo(null)
                .build();
    }

    @Override
    public TipModel inverseConvert(TipModalData data) {
        if(data !=null){

            return TipModel.builder()
                    .id(data.getId())
                    .description(data.getDescription())
                    .photo(null)
                    .build();
        }
        return null;
    }
}
