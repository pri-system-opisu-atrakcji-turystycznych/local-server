package uam.server.local.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.local.data.Data;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserData implements Data {

    private Long id;
    private String email;
    private String password;
    private Long active;
    private Set<RoleData> roles;

}
