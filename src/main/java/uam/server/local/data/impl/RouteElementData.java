package uam.server.local.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.local.data.Data;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RouteElementData implements Data {

    private Long id;
    private RouteData route;
    private ElementData element;
    private Long number;

}
