package uam.server.local.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.local.data.Data;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ElementData implements Data {

    private Long id;
    private String name;
    private String shortDescription;
    private String gpsX;
    private String gpsY;
    private Long photo;
    private byte[] sound;
    private QuestionData question;
    private TipData tip;

}
