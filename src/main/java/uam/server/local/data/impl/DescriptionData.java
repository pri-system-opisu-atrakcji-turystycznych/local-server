package uam.server.local.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.local.data.Data;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DescriptionData implements Data {

    private Long id;
    private ElementData element;
    private String title;
    private String content;
    private Boolean mobile;
    private Boolean web;

}
