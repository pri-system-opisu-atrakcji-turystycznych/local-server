package uam.server.local.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.local.data.Data;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionData implements Data {

    private Long id;
    private String content;
    private String answerOne;
    private String answerTwo;
    private String answerTree;
    private String rightAnswer;

}
