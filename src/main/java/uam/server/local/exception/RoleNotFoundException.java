package uam.server.local.exception;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException(String roleName) {
        super("Role not found for role: " + roleName);
    }
}
