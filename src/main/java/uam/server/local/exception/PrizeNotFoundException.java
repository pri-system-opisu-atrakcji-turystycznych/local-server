package uam.server.local.exception;

public class PrizeNotFoundException extends RuntimeException {
    public PrizeNotFoundException(Long id) {
        super("Prize not found for id: " + id);
    }
}
