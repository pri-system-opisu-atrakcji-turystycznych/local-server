package uam.server.local.exception;

public class MessageNotFoundException extends RuntimeException{
    public MessageNotFoundException(Long id) {
        super("Message not found for id: " + id);
    }
}
