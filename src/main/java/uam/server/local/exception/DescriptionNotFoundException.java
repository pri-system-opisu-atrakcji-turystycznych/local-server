package uam.server.local.exception;

public class DescriptionNotFoundException extends RuntimeException {
    public DescriptionNotFoundException(Long id) {
        super("Description not found for id: " + id);
    }
}
