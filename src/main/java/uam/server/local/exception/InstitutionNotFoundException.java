package uam.server.local.exception;

public class InstitutionNotFoundException extends RuntimeException {
    public InstitutionNotFoundException() {
        super("Institution not found");
    }
}
