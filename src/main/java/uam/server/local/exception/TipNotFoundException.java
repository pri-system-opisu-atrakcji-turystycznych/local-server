package uam.server.local.exception;

public class TipNotFoundException extends RuntimeException {
    public TipNotFoundException(Long id) {
        super("Tip not found for id: " + id);
    }
}
