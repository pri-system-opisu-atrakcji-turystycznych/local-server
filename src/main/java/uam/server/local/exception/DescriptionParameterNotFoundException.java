package uam.server.local.exception;

import uam.server.local.model.DescriptionModel;

public class DescriptionParameterNotFoundException extends Exception {
    public DescriptionParameterNotFoundException(DescriptionModel description) {
        super("Description value not found for description: " + description);
    }
}
