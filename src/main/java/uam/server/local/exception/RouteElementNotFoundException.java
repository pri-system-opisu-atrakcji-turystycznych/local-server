package uam.server.local.exception;

public class RouteElementNotFoundException extends RuntimeException{
    public RouteElementNotFoundException(Long id){  super("Route element not found for id: " + id);
    }
}
