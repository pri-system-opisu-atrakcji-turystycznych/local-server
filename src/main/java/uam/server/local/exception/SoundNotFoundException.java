package uam.server.local.exception;

public class SoundNotFoundException extends RuntimeException {
    public SoundNotFoundException(Long id) {
        super("Sound not found for id: " + id);
    }
}
