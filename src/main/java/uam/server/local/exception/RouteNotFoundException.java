package uam.server.local.exception;

public class RouteNotFoundException extends RuntimeException {
    public RouteNotFoundException(Long id) {
        super("Route not found for id: " + id);
    }
}
