package uam.server.local.exception;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(Long id) {
        super("Image not found for id: " + id);
    }
}
