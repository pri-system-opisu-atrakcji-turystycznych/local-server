package uam.server.local.modalData;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import uam.server.local.data.Data;
import uam.server.local.data.impl.QuestionData;
import uam.server.local.data.impl.TipData;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ElementModalData implements Data {
    private Long id;
    private String name;
    private String shortDescription;
    private String gpsX;
    private String gpsY;
    private MultipartFile photo;
    private QuestionData question;
    private TipData tip;
}
