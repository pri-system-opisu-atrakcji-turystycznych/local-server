package uam.server.local.modalData;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import uam.server.local.data.Data;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RouteModalData implements Data {
    private Long id;
    private String name;
    private String description;
    private MultipartFile photo;
}
