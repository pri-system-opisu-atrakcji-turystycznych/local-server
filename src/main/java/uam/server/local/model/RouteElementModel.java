package uam.server.local.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "RouteElement")
public class RouteElementModel extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "route", referencedColumnName = "id")
    private RouteModel route;

    @ManyToOne
    @JoinColumn(name = "element", referencedColumnName = "id")
    private ElementModel element;

    @Column(name = "number")
    private Long number;

    @Column(name ="removed",columnDefinition = "boolean default false")
    private Boolean removed;

}
