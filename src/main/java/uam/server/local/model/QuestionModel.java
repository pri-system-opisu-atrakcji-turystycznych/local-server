package uam.server.local.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Question")
public class QuestionModel extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "answerOne")
    private String answerOne;

    @Column(name = "answerTwo")
    private String answerTwo;

    @Column(name = "answerTree")
    private String answerTree;

    @Column(name = "rightAnswer")
    private String rightAnswer;

    @Column(name ="removed",columnDefinition = "boolean default false")
    private Boolean removed;

}
