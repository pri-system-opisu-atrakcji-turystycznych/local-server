package uam.server.local.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Element")
public class ElementModel extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "shortDescription", length = 1000)
    private String shortDescription;

    @Column(name = "gpsX")
    private String gpsX;

    @Column(name = "gpsY")
    private String gpsY;

    @Column(name = "photo")
    private Long photo;

    @Column(name = "sound")
    private byte[] sound;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "question", referencedColumnName = "id")
    private QuestionModel question;

    @OneToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "tip", referencedColumnName = "id")
    private TipModel tip;

    @Column(name ="removed",columnDefinition = "boolean default false")
    private Boolean removed;

}
