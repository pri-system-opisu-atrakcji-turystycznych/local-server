package uam.server.local.login.servive;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import uam.server.local.Application;
import uam.server.local.exception.RoleNotFoundException;
import uam.server.local.model.RoleModel;
import uam.server.local.model.UserModel;
import uam.server.local.spring.service.impl.UserService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceIntegrationTestContextConfiguration {
        @Bean
        @Primary
        public UserService userService() {
            return Mockito.mock(UserService.class);
        }
    }

    @Autowired
    private UserService userService;

    @Test
    public void saveUserWithDefiniedRoleBeanTest() {
        //given
        UserModel user = new UserModel();
        user.setEmail("test1@test.pl");
        user.setPassword("asd");
        RoleModel role = new RoleModel();
        role.setRole("ADMIN");
        user.setRoles(new HashSet<RoleModel>(Arrays.asList(role)));

        //when
        UserModel addedUser = userService.saveUser(user);
        Set<RoleModel> addedRoles = addedUser.getRoles();

        //then
        assertNotNull(addedUser);
        assertEquals("ADMIN", addedRoles.iterator().next().getRole());

    }

    @Test
    public void saveUserWithNotDefiniedRoleTest() {
        //given
        UserModel user = new UserModel();
        user.setEmail("test@test.pl");
        user.setPassword("asd");

        //when
        UserModel addedUser = userService.saveUser(user);
        Set<RoleModel> addedRoles = addedUser.getRoles();

        //then
        assertNotNull(addedUser);
        assertEquals("REDAKTOR", addedRoles.iterator().next().getRole());

    }

    @Test
    public void findUserByEmailTest() {
        String emailToFind = "admin@admin.pl";
        //given
        UserModel userToFind = new UserModel();
        userToFind.setEmail("admin@admin.pl");
        //when
        UserModel userFound = userService.findUserByEmail(emailToFind);
        //then
        assertEquals(emailToFind, userFound.getEmail());
    }

    @Test(expected = RoleNotFoundException.class)
    public void saveUserWithWrongDefiniedRoleBeanTest() {
        //given
        UserModel user = new UserModel();
        user.setEmail("test11@test.pl");
        user.setPassword("asd");
        RoleModel role = new RoleModel();
        role.setRole("ADMINaa");
        user.setRoles(new HashSet<RoleModel>(Arrays.asList(role)));

        //when
        UserModel addedUser = userService.saveUser(user);
        Set<RoleModel> addedRoles = addedUser.getRoles();

    }
}