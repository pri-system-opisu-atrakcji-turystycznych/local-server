package uam.server.local.spring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import uam.server.local.spring.service.ImageService;

import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ImageModelControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ImageService imageService;

    @MockBean
    private ImageController imageController;

    @Test
    public void getDemoImage() throws Exception {

        this.mockMvc.perform(get("/image/demo").contentType(IMAGE_JPEG_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getImage() throws Exception {

        this.mockMvc.perform(get("/image/1").contentType(IMAGE_JPEG_VALUE))
                .andExpect(status().isOk());
    }

}
