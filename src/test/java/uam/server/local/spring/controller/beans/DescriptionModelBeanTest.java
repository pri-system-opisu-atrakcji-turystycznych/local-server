package uam.server.local.spring.controller.beans;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import uam.server.local.Application;
import uam.server.local.exception.DescriptionParameterNotFoundException;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;
import uam.server.local.spring.service.ElementService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class DescriptionModelBeanTest {
    //
    @TestConfiguration
    static class DescriptionBeanTestContextConfiguration {
        @Bean
        @Primary
        public ElementService elementService() {
            return Mockito.mock(ElementService.class);
        }
    }

    //
    @Autowired
    private ElementService elementService;

    private static DescriptionBean descriptionBean;
    private DescriptionModel descriptionModel;
    private ElementModel elementModel;

    @BeforeClass
    public static void setup() {
        descriptionBean = new DescriptionBean();

    }

    @Before
    public void init() {
        descriptionBean.setElements(elementService.getAll());
        descriptionModel = new DescriptionModel();
        elementModel = new ElementModel();
        elementModel.setId(1l);
    }

    @Test
    public void prepareDataOptimisticPathTest() throws Exception {
        //given
        descriptionModel.setElement(elementModel);
        descriptionModel.setTitle("title");
        descriptionModel.setContent("some content");
        descriptionModel.setWeb(true);

        //when
        descriptionBean.prepareData(descriptionModel);

        //then
        assertNotNull(descriptionModel.getElement());
        assertEquals("title", descriptionModel.getTitle());
        assertEquals("some content", descriptionModel.getContent());
        assertTrue(descriptionModel.getWeb());
        assertFalse(descriptionModel.getMobile());

    }

    @Test
    public void prepareDataWhenDescriptionNotHaveTitle() throws Exception {
        //given
        descriptionModel.setElement(elementModel);
        descriptionModel.setContent("some content");
        descriptionModel.setWeb(true);

        //when
        descriptionBean.prepareData(descriptionModel);

        //then
        assertEquals("Brak tytułu", descriptionModel.getTitle());
    }

    @Test
    public void prepareDataWhenDescriptionNotHaveContent() throws Exception {
        //given
        descriptionModel.setElement(elementModel);
        descriptionModel.setTitle("title");
        descriptionModel.setWeb(true);

        //when
        descriptionBean.prepareData(descriptionModel);

        //then
        assertEquals("Brak opisu", descriptionModel.getContent());
    }

    @Test
    public void prepareDataWhenDescriptionNotHaveAnyType() throws Exception {
        //given
        descriptionModel.setElement(elementModel);
        descriptionModel.setTitle("title");
        descriptionModel.setContent("some content");
        //when
        descriptionBean.prepareData(descriptionModel);

        //then
        assertFalse(descriptionModel.getWeb());
        assertFalse(descriptionModel.getMobile());
    }

    @Test
    public void prepareDataWhenDescriptionHaveOnlyElement() throws Exception {
        //given
        descriptionModel.setElement(elementModel);

        //when
        descriptionBean.prepareData(descriptionModel);

        //then
        assertEquals("Brak tytułu", descriptionModel.getTitle());
        assertEquals("Brak opisu", descriptionModel.getContent());
        assertFalse(descriptionModel.getWeb());
        assertFalse(descriptionModel.getMobile());
    }

    @Test(expected = DescriptionParameterNotFoundException.class)
    public void prepareDataWhenDescriptionNotHaveElement() throws Exception {
        //given
        descriptionModel.setTitle("title");
        //when
        descriptionBean.prepareData(descriptionModel);
    }
}