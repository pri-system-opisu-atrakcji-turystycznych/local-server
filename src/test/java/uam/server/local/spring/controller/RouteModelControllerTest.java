package uam.server.local.spring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import uam.server.local.spring.service.RouteService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RouteModelControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RouteService routeService;

    @Test
    public void getAllRoutes() throws Exception {

        this.mockMvc.perform(get("/routes").contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void getRoute() throws Exception {

        this.mockMvc.perform(get("/routes/1").contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }

}
