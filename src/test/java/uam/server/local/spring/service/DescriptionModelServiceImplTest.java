package uam.server.local.spring.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import uam.server.local.Application;
import uam.server.local.exception.DescriptionParameterNotFoundException;
import uam.server.local.model.DescriptionModel;
import uam.server.local.model.ElementModel;

import javax.naming.SizeLimitExceededException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class DescriptionModelServiceImplTest {
    @TestConfiguration
    static class DescriptionServiceImplTestContextConfiguration {
        @Bean
        @Primary
        public DescriptionService descriptionService() {
            return Mockito.mock(DescriptionService.class);
        }
    }

    @Autowired
    private DescriptionService descriptionService;

    private DescriptionModel addingDescriptionModel;
    private ElementModel elementModel;

    @Before
    public void init() {
        addingDescriptionModel = new DescriptionModel();
        elementModel = new ElementModel();
        elementModel.setId(1L); //to refactor
    }

    @Test
    public void saveDescriptionReturnedAddedRouteTest() throws Exception {
        //given
        addingDescriptionModel.setElement(elementModel);
        addingDescriptionModel.setTitle("title");
        addingDescriptionModel.setContent("test description content");
        addingDescriptionModel.setWeb(true);
        addingDescriptionModel.setMobile(true);
        //when
        DescriptionModel addedDescriptionModel = descriptionService.save(addingDescriptionModel);
        //then
        assertNotNull(addedDescriptionModel);
    }

    @Test(expected = DescriptionParameterNotFoundException.class)
    public void saveDescriptionThrowsDescriptionParameterNotFoundExceptionWhenWebAndMobileIsNull() throws Exception {
        //given
        addingDescriptionModel.setElement(elementModel);
        addingDescriptionModel.setTitle("title");
        addingDescriptionModel.setContent("test description content");
        //when
        descriptionService.save(addingDescriptionModel);
    }

    @Test(expected = DescriptionParameterNotFoundException.class)
    public void saveDescriptionThrowsDescriptionParameterNotFoundExceptionWhenTitleIsNull() throws Exception {
        //given
        //addingDescriptionModel.setTitle(null);
        addingDescriptionModel.setElement(elementModel);
        addingDescriptionModel.setContent("test description content");
        addingDescriptionModel.setWeb(true);
        //when
        descriptionService.save(addingDescriptionModel);
    }

    @Test(expected = DescriptionParameterNotFoundException.class)
    public void saveDescriptionThrowsDescriptionParameterNotFoundExceptionWhenElementIsNull() throws Exception {
        //given
        addingDescriptionModel.setContent("test description content");
        addingDescriptionModel.setWeb(true);
        //when
        descriptionService.save(addingDescriptionModel);
    }

    @Test(expected = SizeLimitExceededException.class)
    public void saveDescriptionThrowsSizeLimitExceededExceptionContentToLong() throws Exception {
        //given
        addingDescriptionModel.setElement(elementModel);
        addingDescriptionModel.setTitle("title");
        addingDescriptionModel.setContent("Lorem ipsum dolor sit amet, consectetuer adipiscing elit." +
                " Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus " +
                "et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis," +
                " ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis" +
                " enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim" +
                " justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu " +
                "pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. " +
                "Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, " +
                "eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus." +
                " Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. " +
                "Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam" +
                " rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet" +
                " adipiscing sem neque sed ipsum. Na");
        //when
        descriptionService.save(addingDescriptionModel);
    }

    @Test(expected = NullPointerException.class)
    public void saveDescriptionThrowsNullPointerException() throws Exception {
        //given null
        //when
        descriptionService.save(null);
    }

    @Test
    public void saveDescriptionWithGetElementFromRepositoryToJoinDescription() throws Exception {
        //given from init
        addingDescriptionModel.setTitle("test title");
        addingDescriptionModel.setContent("test content");
        addingDescriptionModel.setElement(elementModel);
        addingDescriptionModel.setWeb(true);
        addingDescriptionModel.setMobile(true);
        //when
        DescriptionModel addedDescriptionModel = descriptionService.save(addingDescriptionModel);
        //then
        assertEquals(1, (long) addedDescriptionModel.getElement().getId());
        assertEquals("Budynek dawnej szkoły elementarnej w Luboniu", addedDescriptionModel.getElement().getName());
    }

}