//package com.service;
//
//import SpringBootWebApplication;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.context.annotation.BeanName;
//import org.springframework.context.annotation.Primary;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static org.junit.Assert.*;
//
//@SpringBootTest(classes = SpringBootWebApplication.class)
//@RunWith(SpringRunner.class)
//public class TipServiceImplTest {
//
//    @TestConfiguration
//    static class TipServiceImplTestContextConfiguration {
//        @BeanName
//        @Primary
//        public TipService tipService(){
//            return Mockito.mock(TipService.class);
//        }
//    }
//
//    private TipService tipService;
//}