package uam.server.local.spring.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import uam.server.local.Application;
import uam.server.local.model.RouteModel;

import javax.management.AttributeNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class RouteModelServiceImplTest {

    @TestConfiguration
    static class RouteServiceImplTestContextConfiguration {
        @Bean
        @Primary
        public RouteService routeService() {
            return Mockito.mock(RouteService.class);
        }
    }

    @Autowired
    private RouteService routeService;

    @Test
    public void saveRouteMethodReturnedAddedRouteTest() throws Exception {
        //given
        RouteModel addingRouteModel = new RouteModel();
        addingRouteModel.setName("name of new route");
        addingRouteModel.setDescription("description of new route");
        //when
        RouteModel addedRouteModel = routeService.save(addingRouteModel);
        //then
        assertNotNull(addedRouteModel);
    }

    @Test(expected = NullPointerException.class)
    public void saveRouteNullExceptionTest() throws Exception {
        //given
        RouteModel addingRouteModel = null;
        //when
        RouteModel addedRouteModel = routeService.save(addingRouteModel);

    }

    @Test(expected = AttributeNotFoundException.class)
    public void saveRouteAttributeNotFoundExceptionTest() throws Exception {
        //given
        RouteModel addingRouteModel = new RouteModel();
        //when
        RouteModel addedRouteModel = routeService.save(addingRouteModel);
    }

    @Test
    public void saveRoutSuccessAddTest() throws Exception {
        //given
        RouteModel addingRouteModel = new RouteModel();
        addingRouteModel.setName("name of new test route");
        addingRouteModel.setDescription("description of new route");
        //when
        Long addedRouteId = routeService.save(addingRouteModel).getId();
        RouteModel findAddedRouteModel = routeService.getRouteById(addedRouteId);
        //then
        assertEquals("name of new test route", findAddedRouteModel.getName());

    }
}