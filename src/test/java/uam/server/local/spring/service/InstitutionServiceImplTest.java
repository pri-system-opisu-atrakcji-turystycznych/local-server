//package com.service;
//
//import SpringBootWebApplication;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.context.annotation.BeanName;
//import org.springframework.context.annotation.Primary;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static org.junit.Assert.*;
//
//@SpringBootTest(classes = SpringBootWebApplication.class)
//@RunWith(SpringRunner.class)
//public class InstitutionServiceImplTest {
//
//    @TestConfiguration
//    static class InstitutionServiceImplTestContextConfiguration {
//        @BeanName
//        @Primary
//        public InstitutionService institutionService(){
//            return Mockito.mock(InstitutionService.class);
//        }
//    }
//
//    private InstitutionService institutionService;
//}