#!/bin/sh

mvn clean install

docker image build --force-rm --tag local-serv .

docker run --name local-uam.server -d --rm -p 9002:8080 local-serv

