# Local-server

### Instalowanie
```
    mvn install
```
```
    cd target
```
```    
    java -jar artifact-1.0-SNAPSHOT.war
```


## API Endpoint
|Endpoints|Użycie|Parametry|
|---|---|---|
|```GET /server/status```|Zwraca "Work!" jeśli serwer działa poprawnie.|---|
|```GET /descriptions```|Zwraca wszystkie opisy.|---|
|```GET /descriptions/element/{id}/mobile/{mobile}/web/{web}```|Zwraca opis podanego elementu trasy. Zaleca się ustawianie tylko jednej wartości na true.|{id} - elementu {mobile} - true/false {web} - true/false|
|```GET /institutions```|Zwraca wszystkie opisy instytucji.|---|
|```GET /institutions/main```|Zwraca główny(jeden) opis instytucji.|---|
|```GET /messages```|Zwraca wszystkie wiadomości.|---|
|```GET /elements```|Zwraca wszystkie obiekty.|---|
|```GET /routes```|Zwraca wszystkie trasy.|---|
|```GET /routes/{id}```|Zwraca trasę o podanym id.|{id} - id trasy|
|```GET /routes-elements```|Zwraca wszystkie elementy tras.|---|
|```GET /routes-elements/route/{id}```|Zwraca wszystkie elementy tras - trasy o podanym id.|{id} - id trasy|
|```GET /questions```|Zwraca wszystkie pytania.|---|
|```GET /tips```|Zwraca wszystkie wskazówki.|---|
|```GET /image/demo```|Zwraca demonstracyjny obrazek.|---|
|```GET /image/{id}```|Zwraca obrazek o podanym id.|{id} - id obrazka|
|```GET /sound/demo```|Zwraca demonstracyjny dźwięk.|---|
|```GET /sound/{id}```|Zwraca dźwięk o podanym id.|{id} - id dźwięku|
|```GET /prize```|Zwraca jeden obiekt prize z jednym polem ```code```, jeśli ```"code":null``` to znaczy, że nie ma dostępnych nagród.||


|Endpoints|Użycie|
|---|---|
|```/login```|Interfejs logowania.|